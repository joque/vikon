### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 47bd80b2-8c6a-11eb-0ef5-7ff4b7d86af3
using Pkg; Pkg.add("PlutoUI"); using PlutoUI

# ╔═╡ 9d5526f8-8c6f-11eb-2da0-93e6183e16a7
Pkg.add("Plots"); using Plots

# ╔═╡ 46aaf73c-8c66-11eb-200c-e76bb38d7bbe
1 + 3

# ╔═╡ e687a8e8-8c68-11eb-0c27-9f26c8a67840
println("Welcome to Julia Lang...")

# ╔═╡ c5a9aa56-8c66-11eb-224e-a57c7d70c9ec
begin
	with_terminal() do
		println("Welcome to Julia Lang...")
	end
end

# ╔═╡ bbf52c34-8c6a-11eb-3f42-cbe2aa5e7dce
begin
	student = "Tommy"
	with_terminal() do
		println("Hello $student")
	end
end

# ╔═╡ f24ff598-8c6a-11eb-3e97-e3db95df97b1
begin
	student1 = "Annie"
	student2 = "Ben"
	both_students = student1 * student2
	with_terminal() do
		println(both_students)
	end
end

# ╔═╡ 6b1467d6-8c6c-11eb-3b3d-f7bd5eeae1f1
begin
	a1 = [1,2,3,4,5]
	a1
	a2 = [[1,2,3,4] [5,6,7,8] [9,10,11,12]]
	a2
end

# ╔═╡ b3b6894c-8c6c-11eb-3cc2-79248953f16f
begin
	student_one = Dict("name" => "James", "year" => 3, "subject" => "CS")
	student_one
	student_one["year"]
end

# ╔═╡ 2b0b8a3e-8c6e-11eb-3f5e-4323f9dac21d
begin
	wednesday_acts = ("ai-practical", "meeting1", "meeting2")
	wednesday_acts
	wednesday_acts[2]
end

# ╔═╡ 38145b78-8c70-11eb-1e82-cf4640a47fd4
plot(randn(200))

# ╔═╡ ea6cba50-8c74-11eb-0ca5-3f514fb2cdeb
begin
	struct Student
		name::String
		year::Int64
		subjects::Array{String, 1}
	end
	
	name(st::Student) = st.name
	year_of_study(st::Student) = st.year
	
	marc = Student("Marc Angelo", 2, ["AI", "Programming", "DSP"])
	marc
end

# ╔═╡ Cell order:
# ╠═47bd80b2-8c6a-11eb-0ef5-7ff4b7d86af3
# ╠═46aaf73c-8c66-11eb-200c-e76bb38d7bbe
# ╠═e687a8e8-8c68-11eb-0c27-9f26c8a67840
# ╠═c5a9aa56-8c66-11eb-224e-a57c7d70c9ec
# ╠═bbf52c34-8c6a-11eb-3f42-cbe2aa5e7dce
# ╠═f24ff598-8c6a-11eb-3e97-e3db95df97b1
# ╠═6b1467d6-8c6c-11eb-3b3d-f7bd5eeae1f1
# ╠═b3b6894c-8c6c-11eb-3cc2-79248953f16f
# ╠═2b0b8a3e-8c6e-11eb-3f5e-4323f9dac21d
# ╠═9d5526f8-8c6f-11eb-2da0-93e6183e16a7
# ╠═38145b78-8c70-11eb-1e82-cf4640a47fd4
# ╠═ea6cba50-8c74-11eb-0ca5-3f514fb2cdeb
