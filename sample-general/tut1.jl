### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 4a6b5b1e-8c7a-11eb-3aee-ed3ead088b3d
using Pkg; Pkg.add("PlutoUI"); using PlutoUI

# ╔═╡ 1a18d342-8c7a-11eb-280d-e5bb96a33f3a
1 + 3

# ╔═╡ 333ddfb4-8c7a-11eb-2169-3d0274927900
with_terminal() do
	println("Hello World")
end

# ╔═╡ a522bb6a-8c7a-11eb-3b11-17410ca5e4f6
begin
	name = "Bob"
	with_terminal() do
		println("Welcome to Julia Tutorial! $name")
	end
end

# ╔═╡ 6eba73ae-8c7c-11eb-3e1d-811d40757cee
val = 55

# ╔═╡ 85846054-8c7c-11eb-0ee9-33572c914d37
with_terminal() do
	println(val)
end

# ╔═╡ b03bae24-8c7c-11eb-322f-1357bbe00f4f
courses = Dict("dsp" => "Distributed Systems Programming", "ai" => "Artificial Intelligence", "svt" => "System Virtualisation")

# ╔═╡ e8be1fe8-8c7c-11eb-0b3b-25cd46638adc
courses["ai"]

# ╔═╡ 17ad9dba-8c7d-11eb-3c58-1b8a870c7e4a
semester_courses = ("aig", "progr2", "WIL")

# ╔═╡ 45ecba9e-8c7d-11eb-2d92-8301cc82d1b2
semester_courses[2]

# ╔═╡ 6f624862-8c7d-11eb-3be7-89f843eed592
a1 = [1,2,3,4,5]

# ╔═╡ 91312d50-8c7d-11eb-2e25-a374f3cbbea6
a1[2]

# ╔═╡ a074c3da-8c7d-11eb-04ab-d9ab6a3555e1
push!(a1, 8)

# ╔═╡ bc00809e-8c7d-11eb-042a-5990ee753de1
a1

# ╔═╡ caaa726c-8c7d-11eb-0613-5f8eb4c0179e
pop!(a1)

# ╔═╡ e79bae54-8c7d-11eb-2e06-d1873cd6e0a7
a1

# ╔═╡ f32f6576-8c7d-11eb-2b8f-e5d5e272d49e
begin
	push!(a1, 4)
	push!(a1, 9)
end

# ╔═╡ 3680e28c-8c7e-11eb-058c-9365c0a3da50
length(a1)

# ╔═╡ f37b58d6-8c7e-11eb-1e8c-7feab592969f
a2 = [[1,2,3,4] [5,6,7,8] [9, 10, 11, 12]]

# ╔═╡ 4110a554-8c7f-11eb-3f13-7d87c9b85155
length(a2)

# ╔═╡ 7b7361d4-8c7f-11eb-3f1a-a18f0dbd2a92
a3 = rand(4,3)

# ╔═╡ 91e064bc-8c7f-11eb-0216-39dbf74b48d5
a4 = zeros(4,3)

# ╔═╡ b025f22a-8c7f-11eb-09e0-31746c552ecf
a5 = ones(4,3)

# ╔═╡ 05a17c34-8c81-11eb-2bed-7b8477aaae08
with_terminal() do
	println(a1)
end

# ╔═╡ 4a8c6e60-8c81-11eb-3a12-7547c2244358
a1[1]

# ╔═╡ 887544a4-8c81-11eb-2039-8323695f80c5
begin
	counter1 = 2
	a1[counter1]
end

# ╔═╡ d395bbfa-8c7f-11eb-33d1-79dddb4c26cb

begin
	with_terminal() do
		counter = 1
		while counter < length(a1)
			println(a1[counter])
			counter += 1
		end
	end
end

# ╔═╡ f5388c22-8c81-11eb-33f8-bf62db5af4f7
begin
	with_terminal() do
		for row_val in 1:4
			for col_val in 1:3
				println(a2[row_val, col_val])
			end
			println("\n")
		end
	end
end

# ╔═╡ Cell order:
# ╠═1a18d342-8c7a-11eb-280d-e5bb96a33f3a
# ╠═4a6b5b1e-8c7a-11eb-3aee-ed3ead088b3d
# ╠═333ddfb4-8c7a-11eb-2169-3d0274927900
# ╠═a522bb6a-8c7a-11eb-3b11-17410ca5e4f6
# ╠═6eba73ae-8c7c-11eb-3e1d-811d40757cee
# ╠═85846054-8c7c-11eb-0ee9-33572c914d37
# ╠═b03bae24-8c7c-11eb-322f-1357bbe00f4f
# ╠═e8be1fe8-8c7c-11eb-0b3b-25cd46638adc
# ╠═17ad9dba-8c7d-11eb-3c58-1b8a870c7e4a
# ╠═45ecba9e-8c7d-11eb-2d92-8301cc82d1b2
# ╠═6f624862-8c7d-11eb-3be7-89f843eed592
# ╠═91312d50-8c7d-11eb-2e25-a374f3cbbea6
# ╠═a074c3da-8c7d-11eb-04ab-d9ab6a3555e1
# ╠═bc00809e-8c7d-11eb-042a-5990ee753de1
# ╠═caaa726c-8c7d-11eb-0613-5f8eb4c0179e
# ╠═e79bae54-8c7d-11eb-2e06-d1873cd6e0a7
# ╠═f32f6576-8c7d-11eb-2b8f-e5d5e272d49e
# ╠═3680e28c-8c7e-11eb-058c-9365c0a3da50
# ╠═f37b58d6-8c7e-11eb-1e8c-7feab592969f
# ╠═4110a554-8c7f-11eb-3f13-7d87c9b85155
# ╠═7b7361d4-8c7f-11eb-3f1a-a18f0dbd2a92
# ╠═91e064bc-8c7f-11eb-0216-39dbf74b48d5
# ╠═b025f22a-8c7f-11eb-09e0-31746c552ecf
# ╠═05a17c34-8c81-11eb-2bed-7b8477aaae08
# ╠═4a8c6e60-8c81-11eb-3a12-7547c2244358
# ╠═887544a4-8c81-11eb-2039-8323695f80c5
# ╠═d395bbfa-8c7f-11eb-33d1-79dddb4c26cb
# ╠═f5388c22-8c81-11eb-33f8-bf62db5af4f7
