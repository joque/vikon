Introduction to Julia
=====================

This document presents an overview of the **Julia** programming language.

# Installation

To install **Julia**, first download it (https://julialang.org/downloads) and then follow the installation instructions based on your operating system. Note that the current stable version is v1.5.4 and that you can install on MacOS, Windows and Linux. If you wish, you can also download the source code and compile it.

# Documentation \& Support

You can access the official documentation at https://docs.julialang.org/en./v1. If you need further support in Julia, you can join the official channels on Discourse and Slack.

# Environment

Once you install **Julia**, you can run programs written in it from the terminal or from specific IDEs, including [Juno](https://junolab.org/). In this course, we will use a notebook called [Pluto](https://github.com/fonsp/Pluto.jl).

## Task
1. Install Julia on your machine
2. Install Pluto
	1. Start Julia in the terminal
	1. switch to package mode (press ])
	1. type 'add Pluto'
	1. Use Ctrl + c to leave the package mode
	1. type 'import Pluto'
	1. type 'Pluto.run()'

In the last instruction is successful, you will get a URL, which you can open in your browser and execute Julia code.

# Basics

Name your first notebook basics and save it in a folder of your choice.

## Tasks

- print the string 'Welcome to Julia Lang...'
- assign values to variables and interpolate them

# Data Structures

 - Dictionary
 - Tuple
 - Array (vector and multi-dimensional)

# Loops

- While
- For

# Conditionals

# Functions

# Packages

# Class
