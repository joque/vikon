### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ c7efdeca-75d5-4db0-88cf-a0b6efa4b220
using Pkg

# ╔═╡ 0f0b3bac-cfff-48ce-89ca-79e6237c1bdb
Pkg.activate("Project.toml")

# ╔═╡ 81422486-b3ee-492c-b3f9-e218ab7f80c6
using PlutoUI

# ╔═╡ eb13c655-0f69-4753-9ca5-6bf30fef60de
using DataStructures

# ╔═╡ 9a56f850-9b97-11eb-2151-dfc59ba4a39c
md"# Lab2"

# ╔═╡ b16878cb-8fd5-4b6e-b356-101a9f1e0d33
struct Action
	Name::String
	cost::Int64
end

# ╔═╡ aa5b34f9-d10d-44e1-8667-c80305630ef3
Right = Action("move_Right", 1)

# ╔═╡ e645511c-a835-4dbb-a0b1-973d7418fbe6
Left = Action("move_Left", 1) 

# ╔═╡ c40d5cc6-c4c0-4145-8c0e-d0f7540e1a47
Pick = Action("pick_up", 3)

# ╔═╡ af64fef0-2e86-4998-98dc-176c65b19bd1
struct State
	Name::String
	Condition::Vector{Bool}
	Position::Int64
end

# ╔═╡ 4c83acf7-44cc-4a49-ad30-f39c3bab6bc1
Zero = State("State0", [true, true, true], 1)

# ╔═╡ a69a7d49-01a3-4c07-9cb0-465d1b262ff3
Zero2 = State("State02", [true, true, true], 2)

# ╔═╡ ef104f1a-1617-492d-8c77-4804056f2232
Zero3 = State("State03", [true, true, true], 3)

# ╔═╡ 5a496a14-c9e7-44bd-b30d-c54d2f782148
One = State("State1", [false, true, true], 1)

# ╔═╡ fb78b458-4725-4bcf-ac77-cf1ff7c0a916
One2 = State("State1.2", [false, true, true], 2)

# ╔═╡ d7550f11-7ae3-4c91-8526-a71646555bc7
One3 = State("State1.3", [false, true, true], 3)

# ╔═╡ e16a720d-6cb0-47aa-ba7e-76c875e8d1a5
Two = State("State2", [false, false, true], 2)

# ╔═╡ e544c725-aaf8-46d1-b629-6b5768e07091
Two2 = State("State2.2", [false, false, true], 1)

# ╔═╡ 54514f87-35b1-4829-a02d-2e0b9bc50b04
Two3 = State("State2.3", [false, false, true], 3)

# ╔═╡ 22ce8102-28b7-402d-a2a5-c3f51f4dfe7f
Three = State("State3", [true, false, true], 2)

# ╔═╡ 159ba5d7-3b7e-4ce1-bf87-64415598ba13
Three1 = State("State3.1", [true, false, true], 3)

# ╔═╡ 549fbd35-c441-4eeb-99c0-8c601fc60332
Three2 = State("State3.2", [true, false, true], 1)

# ╔═╡ 7bbb80b1-355f-44fa-9bb0-45923c5bebe0
Four = State("State4", [true, true, false], 3)

# ╔═╡ bc626e95-4220-495e-988b-7f0a7448fc9f
Four2 = State("State4.2", [true, true, false], 2)

# ╔═╡ a100a893-0ff5-41a4-8644-4b5556855aeb
Four3 = State("State4.3", [true, true, false], 1)

# ╔═╡ 54321d57-c032-40d7-9c3c-082025413b25
Goal = State("Goal", [false, true, false], 3)

# ╔═╡ c75b2140-4f9b-43ed-b7e2-1a6d6905bdf8
trans_model = Dict(Zero =>[(Pick, One), (Right, Zero2), (Left, Zero)])

# ╔═╡ babe98f6-f1ce-429a-ab4a-870bea41be14
push!(trans_model, One => [(Left, One), (Right, One2)])

# ╔═╡ 6697011f-b310-4997-940f-8dbc14da20dc
push!(trans_model, One2 => [(Left, One), (Right, One3), (Pick, Two)])

# ╔═╡ 17e87b7b-a890-4364-92db-4e42b3987b82
push!(trans_model, One3 => [(Pick, Goal)])

# ╔═╡ 726d8955-b19f-49fa-8f03-8d7a1e9fa89d
push!(trans_model, Two => [(Right, Two3), (Left, Two2)])

# ╔═╡ d89775a5-9510-463a-a245-39fee5ae2d02
push!(trans_model, Zero2 =>[(Pick, Three), (Left, Zero), (Right, Zero3)])

# ╔═╡ 0a99b856-8972-4182-8275-5b2bcb6b95eb
push!(trans_model, Zero3 => [(Pick, Four), (Right, Zero3)])

# ╔═╡ d9490e18-932b-4f90-8293-62091ef19895
push!(trans_model, Three =>[ (Right, Three2), (Left, Three1)])

# ╔═╡ 21d18d6f-9cba-49c4-8c67-3dafdf662f8a
push!(trans_model, Four => [(Right, Four), (Left, Four2)])

# ╔═╡ cc0ced05-7c03-4a51-ba4b-c693b9f8eff7
push!(trans_model, Four2 => [(Right, Four), (Left, Four3)])

# ╔═╡ f07d6672-f192-446e-abb5-bbb67ec80b24
md"# Breath-first Search strategy"

# ╔═╡ 0a73efb6-4ca8-44fe-9b13-a8eba0d42dce
begin
	with_terminal() do 
		for e in trans_model[Zero]	
		println(e)
	end
		
	end		
end

# ╔═╡ 3c08192c-03a1-4dda-8ce5-127e39b9cdd6
function create_result(Transition_Mod, explored, goal_state)
	result = []
	for i in 1:(length(explored) - 1)
		comp = Transition_Mod[explored[i]]
		for element in comp
			if i == (length(explored) - 1)
				if element[2] == goal_state
					push!(result, element[1])
				end
			else
				if element[2] == explored[i+1]
					push!(result, element[1])
				end
				
			end
		end
	end
	return result
end	

# ╔═╡ 711fe457-cbed-4e97-946c-ff83c16c0ad5
function bfs(initial_state, transition_model, goal_states)
	explored = []
	frontier = Queue{State}()
	enqueue!(frontier, initial_state)
	
	while true
		if isempty(frontier)
			return []
		else
			current_state = dequeue!(frontier)
			push!(explored, current_state)
			nextStates = transition_model[current_state]			
			for e in nextStates
				if !(e[2] in explored) 
					if (e[2] in goal_states)
						return create_result(transition_model, explored, e[2])
					else
						enqueue!(frontier, e[2])
					end
				end
			
			end
		end		
		
	end
end

# ╔═╡ aee2362e-c4f3-410f-ab83-81101aa94936
bfs(Zero, trans_model, [Goal])

# ╔═╡ Cell order:
# ╠═9a56f850-9b97-11eb-2151-dfc59ba4a39c
# ╠═c7efdeca-75d5-4db0-88cf-a0b6efa4b220
# ╠═0f0b3bac-cfff-48ce-89ca-79e6237c1bdb
# ╠═81422486-b3ee-492c-b3f9-e218ab7f80c6
# ╠═b16878cb-8fd5-4b6e-b356-101a9f1e0d33
# ╠═aa5b34f9-d10d-44e1-8667-c80305630ef3
# ╠═e645511c-a835-4dbb-a0b1-973d7418fbe6
# ╠═c40d5cc6-c4c0-4145-8c0e-d0f7540e1a47
# ╠═af64fef0-2e86-4998-98dc-176c65b19bd1
# ╠═4c83acf7-44cc-4a49-ad30-f39c3bab6bc1
# ╠═a69a7d49-01a3-4c07-9cb0-465d1b262ff3
# ╠═ef104f1a-1617-492d-8c77-4804056f2232
# ╠═5a496a14-c9e7-44bd-b30d-c54d2f782148
# ╠═fb78b458-4725-4bcf-ac77-cf1ff7c0a916
# ╠═d7550f11-7ae3-4c91-8526-a71646555bc7
# ╠═e16a720d-6cb0-47aa-ba7e-76c875e8d1a5
# ╠═e544c725-aaf8-46d1-b629-6b5768e07091
# ╠═54514f87-35b1-4829-a02d-2e0b9bc50b04
# ╠═22ce8102-28b7-402d-a2a5-c3f51f4dfe7f
# ╠═159ba5d7-3b7e-4ce1-bf87-64415598ba13
# ╠═549fbd35-c441-4eeb-99c0-8c601fc60332
# ╠═7bbb80b1-355f-44fa-9bb0-45923c5bebe0
# ╠═bc626e95-4220-495e-988b-7f0a7448fc9f
# ╠═a100a893-0ff5-41a4-8644-4b5556855aeb
# ╠═54321d57-c032-40d7-9c3c-082025413b25
# ╠═c75b2140-4f9b-43ed-b7e2-1a6d6905bdf8
# ╠═babe98f6-f1ce-429a-ab4a-870bea41be14
# ╠═6697011f-b310-4997-940f-8dbc14da20dc
# ╠═17e87b7b-a890-4364-92db-4e42b3987b82
# ╠═726d8955-b19f-49fa-8f03-8d7a1e9fa89d
# ╠═d89775a5-9510-463a-a245-39fee5ae2d02
# ╠═0a99b856-8972-4182-8275-5b2bcb6b95eb
# ╠═d9490e18-932b-4f90-8293-62091ef19895
# ╠═21d18d6f-9cba-49c4-8c67-3dafdf662f8a
# ╠═cc0ced05-7c03-4a51-ba4b-c693b9f8eff7
# ╟─f07d6672-f192-446e-abb5-bbb67ec80b24
# ╠═eb13c655-0f69-4753-9ca5-6bf30fef60de
# ╠═0a73efb6-4ca8-44fe-9b13-a8eba0d42dce
# ╠═711fe457-cbed-4e97-946c-ff83c16c0ad5
# ╠═3c08192c-03a1-4dda-8ce5-127e39b9cdd6
# ╠═aee2362e-c4f3-410f-ab83-81101aa94936
