### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 9cd76f12-a1b2-11eb-105f-1f0b6c2c0808
using Pkg

# ╔═╡ 5ec460ce-30c6-4359-9cc0-4501c194bb26
Pkg.activate("Project.toml")

# ╔═╡ 1976d3b5-9b63-4d55-8905-ec656b5b83c3
using Plots

# ╔═╡ 30a9bdf7-ffc7-4234-b0ba-1d583224a7bb
md"# Ploting Functions"

# ╔═╡ 2bd8ee20-435d-4601-b2ed-fac229c29886
x = 1:30

# ╔═╡ 54a859a9-bbda-43bb-98dd-f518fc4b5292
typeof(x)

# ╔═╡ d3279fc9-6c36-4d74-b38a-42faf103737b
y = exp.(x)./sum(exp.(x))

# ╔═╡ d6a0564e-0ca0-451f-92f6-c20b343ac001
gr()

# ╔═╡ 3d3a3d18-a92d-4879-9aeb-824e613261ac
plot(x, y, title="Softmax function")

# ╔═╡ 220e3be5-9226-4d23-b5b3-a284da258d2f
savefig("softmax.png")

# ╔═╡ Cell order:
# ╟─30a9bdf7-ffc7-4234-b0ba-1d583224a7bb
# ╠═9cd76f12-a1b2-11eb-105f-1f0b6c2c0808
# ╠═5ec460ce-30c6-4359-9cc0-4501c194bb26
# ╠═1976d3b5-9b63-4d55-8905-ec656b5b83c3
# ╠═2bd8ee20-435d-4601-b2ed-fac229c29886
# ╠═54a859a9-bbda-43bb-98dd-f518fc4b5292
# ╠═d3279fc9-6c36-4d74-b38a-42faf103737b
# ╠═d6a0564e-0ca0-451f-92f6-c20b343ac001
# ╠═3d3a3d18-a92d-4879-9aeb-824e613261ac
# ╠═220e3be5-9226-4d23-b5b3-a284da258d2f
