### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 89503018-b0ac-11eb-2ee6-ad574840a4b5
using Pkg

# ╔═╡ 3737b39f-15e7-43e4-af04-8757df724055
Pkg.activate("Project.toml")

# ╔═╡ 29e6995f-0c0c-46d6-8ba7-bca970bc7cd8
using CSV

# ╔═╡ 707aa813-bc88-42b1-a816-bf8af83bc1ae
using DataFrames

# ╔═╡ b25898e8-916f-43ed-94a0-6023d57fbafb
using PlutoUI

# ╔═╡ bb7e76b2-171c-426c-a7da-c422e5cac4cf
using RDatasets

# ╔═╡ c42174cf-af0c-4a4b-8e6c-142037db8f6c
using StatsBase

# ╔═╡ 01775cef-88f8-4633-b841-9e8430a1a354
using Statistics

# ╔═╡ 1d74e708-54fb-4189-8ea4-219275f61695
using Plots

# ╔═╡ 1613ee03-ace3-4c15-a99f-535b005f8d6d
md"# Linear Regression Model from scratch in Julia"

# ╔═╡ f3b85c2b-b4d9-49ce-9bd7-74e907b6e757
md"## Import libraries"

# ╔═╡ 1a83a425-bed6-48e1-a25a-f6c64985e830
md"## Load the dataset"

# ╔═╡ 951018d1-ef1f-4742-b69b-c9ab59f70d01
res_df = CSV.read("data/real_estate.csv", DataFrame)

# ╔═╡ 1ff34058-2926-49a9-88c0-a716c7c282d5
begin
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\[m\]"=>"_"))))
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\[s\]"=>"_"))))
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\s"=>"_"))))
end

# ╔═╡ 8911f500-5667-4ffe-8e2d-d3b12e91c97e
md"## Quick exploration of the dataset"

# ╔═╡ 9e900796-63dd-4e5d-87ef-2cfaac307dff
md"### size and column names"

# ╔═╡ 278406b6-585a-4414-a408-ceb71fef6823
size(res_df)

# ╔═╡ 1a2f253a-01b2-43c6-a32c-f54e838a6041
with_terminal() do
	for col_name in names(res_df)
		println(col_name)
	end
end

# ╔═╡ 6b28432b-255f-4262-830d-e5b47bce6525
first(res_df, 15)

# ╔═╡ 79b59bb3-f9a9-4d08-93ed-9d65fc469240
with_terminal() do
	describe(res_df[!, :X1_transaction_date])
end

# ╔═╡ 7f4cb045-761b-4c80-972a-9193b64c1ab6
with_terminal() do
	describe(res_df[!, :X2_house_age])
end

# ╔═╡ 63950354-4c2f-4595-bf0f-705043ee8348
with_terminal() do
	describe(res_df[!, :X3_distance_to_the_nearest_MRT_station])
end

# ╔═╡ ed8c7c33-9af1-4364-b6db-4762432fb371
with_terminal() do
	describe(res_df[!, :X4_number_of_convenience_stores])
end

# ╔═╡ 7d39176e-d9f0-45b2-bf58-257fd856a2eb
with_terminal() do
	describe(res_df[!, :X5_latitude])
end

# ╔═╡ 9051b0c6-7007-403b-aec5-20a66c1f3515
with_terminal() do
	describe(res_df[!, :X6_longitude])
end

# ╔═╡ 67c3ae9b-5db6-4da8-9cb5-c7c742726bfc
with_terminal() do
	describe(res_df[!, :Y_house_price_of_unit_area])
end

# ╔═╡ 5456b1fa-9dcc-4eac-ad2b-9f33bc947f76
cor(Matrix(res_df))

# ╔═╡ a611bc0b-d734-47ac-b915-b6b0dcb28df8
md""" The describe function provides some statistical information that helps understand each column in the dataset. However, it might not be enough to decide which column to select as a feature. Several techniques can be used to assist in feature selection. For example, here we use the Pearson correlation between variables in the dataset. This confirms our initial intuition that columns 3 to 7 could serve as features based on their correlation with the last column.
"""

# ╔═╡ 090d0063-bbe0-4d5c-8026-1338ff527c01
md"## Prepare the model"

# ╔═╡ 716df12f-c6f9-48e3-b2e4-3fb18e13df76
md"### Extract training and testing datasets"

# ╔═╡ 2915577e-49fc-404a-af0c-d275134fd9c3
X = res_df[:, 3:7]

# ╔═╡ e3e6980b-53fe-4013-95aa-21f948e01b90
X_mat = Matrix(X)

# ╔═╡ 2df495ed-0a9e-43fb-a746-c29f8887fc34
Y = res_df[:,8]

# ╔═╡ 9caaa4d8-59b8-4ab5-bb4a-3ff9a2ef0707
Y_mat = Vector(Y)

# ╔═╡ c65ef053-8cbb-4f36-a1b9-a9fd8c4ac30a
training_size = 0.7

# ╔═╡ c4ab36ae-366d-4230-ba08-831c201eac91
all_data_size = size(X_mat)[1]

# ╔═╡ 2d4475d6-842b-4284-80f0-8c3fbbc4974d
training_index = trunc(Int, training_size * all_data_size)

# ╔═╡ d7a75646-34c7-4f19-92f5-c39700253977
X_mat_train = X_mat[1:training_index, :]

# ╔═╡ 201945de-99fc-4987-ac2b-0e4cdce784f0
X_mat_test = X_mat[training_index+1:end, :]

# ╔═╡ da1ffefe-e2b5-4e9e-ae71-a542cc878efb
Y_mat_train = Y_mat[1:training_index]

# ╔═╡ b05a918e-1f02-4dff-a2d2-3dbaeb162eb0
Y_mat_test = Y_mat[training_index+1:end]

# ╔═╡ b9a39a3d-f524-48b9-87f8-f9b042a63f79
function get_loss(feature_mat, outcome_vec, weights)
	m = size(feature_mat)[1]
	hypothesis = feature_mat * weights
	loss = hypothesis - outcome_vec
	cost = (1/(2m)) * (loss' * loss)
	return cost
end

# ╔═╡ 51d831b5-077a-4326-a15b-827517e80ca5
function get_scaling_params(init_feature_mat)
	feature_mean = mean(init_feature_mat, dims=1)
	f_dev = std(init_feature_mat, dims=1)
	return (feature_mean, f_dev)
end

# ╔═╡ 590934c5-7cfc-468e-99e6-3f4a08b61cd9
function scale_features(feature_mat, sc_params)
	normalised_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 928e2da1-02d0-4003-991c-a3a94dd88e19
scaling_params = get_scaling_params(X_mat_train)

# ╔═╡ 0dd2f1e9-1401-4923-a18b-2b49db5b2109
scaling_params[1]

# ╔═╡ 0c4ceb28-3341-4c7c-a5c1-037d88231d0b
scaling_params[2]

# ╔═╡ 60d1c981-c075-4b5d-8179-ad7d01077651
scaled_training_features = scale_features(X_mat_train, scaling_params)

# ╔═╡ 8e551a17-5bc4-4c29-ba69-de4ccfaffc19
scaled_testing_features = scale_features(X_mat_test, scaling_params)

# ╔═╡ b0b2f6cf-9ca2-4d00-88d3-cb3925ed137b
function train_model(features, outcome, alpha, n_iter)
	total_entry_count = length(outcome)
	aug_features = hcat(ones(total_entry_count, 1), features)
	feature_count = size(aug_features)[2]
	weights = zeros(feature_count)
	loss_vals = zeros(n_iter)
	for i in range(1, stop=n_iter)
		pred = aug_features * weights
		loss_vals[i] = get_loss(aug_features, outcome, weights)
		weights = weights - ((alpha/total_entry_count) * aug_features') * (pred - outcome)
	end
	return (weights, loss_vals)
end

# ╔═╡ bd69ea0d-3c6c-4f21-a93d-0a658b495239
weights_tr_errors = train_model(scaled_training_features, Y_mat_train, 0.03,4000)

# ╔═╡ e040303d-a20a-4eb7-9ced-89fd574362d7
plot(weights_tr_errors[2], 
     label="Cost", 
     ylabel="Cost", 
     xlabel="Number of Iteration",
     title="Cost Per Iteration")

# ╔═╡ 788373ee-5acb-4916-b485-65f0c1e13012
function get_predictions(features, weights)
	total_entry_count = size(features)[1]
	aug_features = hcat(ones(total_entry_count, 1), features)
	preds = aug_features * weights
	return preds
end

# ╔═╡ 2cea3ff9-a73d-4f8b-a4be-ac3c2ea25651
function root_mean_square_error(actual_outcome, predicted_outcome)
	errors = predicted_outcome - actual_outcome
	squared_errors = errors .^ 2
	mean_squared_errors = mean(squared_errors)
	rmse = sqrt(mean_squared_errors)
	return rmse
end

# ╔═╡ 30f9608c-c18b-4f92-8ac6-32498113e5d9
training_predictions = get_predictions(scaled_training_features, weights_tr_errors[1])

# ╔═╡ 424be0a2-2b0a-41d7-9239-cbef20c7574b
testing_predictions = get_predictions(scaled_testing_features,weights_tr_errors[1])

# ╔═╡ 8c9c1052-58b5-4d90-b90f-50243484e8a6
root_mean_square_error(Y_mat_train, training_predictions)

# ╔═╡ b2cbd2de-ee5d-4c0f-91c8-9f0a596bd908
root_mean_square_error(Y_mat_test, testing_predictions)

# ╔═╡ Cell order:
# ╠═1613ee03-ace3-4c15-a99f-535b005f8d6d
# ╠═f3b85c2b-b4d9-49ce-9bd7-74e907b6e757
# ╠═89503018-b0ac-11eb-2ee6-ad574840a4b5
# ╠═3737b39f-15e7-43e4-af04-8757df724055
# ╠═29e6995f-0c0c-46d6-8ba7-bca970bc7cd8
# ╠═707aa813-bc88-42b1-a816-bf8af83bc1ae
# ╠═b25898e8-916f-43ed-94a0-6023d57fbafb
# ╠═bb7e76b2-171c-426c-a7da-c422e5cac4cf
# ╠═c42174cf-af0c-4a4b-8e6c-142037db8f6c
# ╠═01775cef-88f8-4633-b841-9e8430a1a354
# ╠═1d74e708-54fb-4189-8ea4-219275f61695
# ╠═1a83a425-bed6-48e1-a25a-f6c64985e830
# ╠═951018d1-ef1f-4742-b69b-c9ab59f70d01
# ╠═1ff34058-2926-49a9-88c0-a716c7c282d5
# ╠═8911f500-5667-4ffe-8e2d-d3b12e91c97e
# ╠═9e900796-63dd-4e5d-87ef-2cfaac307dff
# ╠═278406b6-585a-4414-a408-ceb71fef6823
# ╠═1a2f253a-01b2-43c6-a32c-f54e838a6041
# ╠═6b28432b-255f-4262-830d-e5b47bce6525
# ╠═79b59bb3-f9a9-4d08-93ed-9d65fc469240
# ╠═7f4cb045-761b-4c80-972a-9193b64c1ab6
# ╠═63950354-4c2f-4595-bf0f-705043ee8348
# ╠═ed8c7c33-9af1-4364-b6db-4762432fb371
# ╠═7d39176e-d9f0-45b2-bf58-257fd856a2eb
# ╠═9051b0c6-7007-403b-aec5-20a66c1f3515
# ╠═67c3ae9b-5db6-4da8-9cb5-c7c742726bfc
# ╠═5456b1fa-9dcc-4eac-ad2b-9f33bc947f76
# ╠═a611bc0b-d734-47ac-b915-b6b0dcb28df8
# ╠═090d0063-bbe0-4d5c-8026-1338ff527c01
# ╠═716df12f-c6f9-48e3-b2e4-3fb18e13df76
# ╠═2915577e-49fc-404a-af0c-d275134fd9c3
# ╠═e3e6980b-53fe-4013-95aa-21f948e01b90
# ╠═2df495ed-0a9e-43fb-a746-c29f8887fc34
# ╠═9caaa4d8-59b8-4ab5-bb4a-3ff9a2ef0707
# ╠═c65ef053-8cbb-4f36-a1b9-a9fd8c4ac30a
# ╠═c4ab36ae-366d-4230-ba08-831c201eac91
# ╠═2d4475d6-842b-4284-80f0-8c3fbbc4974d
# ╠═d7a75646-34c7-4f19-92f5-c39700253977
# ╠═201945de-99fc-4987-ac2b-0e4cdce784f0
# ╠═da1ffefe-e2b5-4e9e-ae71-a542cc878efb
# ╠═b05a918e-1f02-4dff-a2d2-3dbaeb162eb0
# ╠═b9a39a3d-f524-48b9-87f8-f9b042a63f79
# ╠═51d831b5-077a-4326-a15b-827517e80ca5
# ╠═590934c5-7cfc-468e-99e6-3f4a08b61cd9
# ╠═928e2da1-02d0-4003-991c-a3a94dd88e19
# ╠═0dd2f1e9-1401-4923-a18b-2b49db5b2109
# ╠═0c4ceb28-3341-4c7c-a5c1-037d88231d0b
# ╠═60d1c981-c075-4b5d-8179-ad7d01077651
# ╠═8e551a17-5bc4-4c29-ba69-de4ccfaffc19
# ╠═b0b2f6cf-9ca2-4d00-88d3-cb3925ed137b
# ╠═bd69ea0d-3c6c-4f21-a93d-0a658b495239
# ╠═e040303d-a20a-4eb7-9ced-89fd574362d7
# ╠═788373ee-5acb-4916-b485-65f0c1e13012
# ╠═2cea3ff9-a73d-4f8b-a4be-ac3c2ea25651
# ╠═30f9608c-c18b-4f92-8ac6-32498113e5d9
# ╠═424be0a2-2b0a-41d7-9239-cbef20c7574b
# ╠═8c9c1052-58b5-4d90-b90f-50243484e8a6
# ╠═b2cbd2de-ee5d-4c0f-91c8-9f0a596bd908
