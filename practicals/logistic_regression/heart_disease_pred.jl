### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ d3bc4664-1270-4e21-aac6-e7a9a7d9f5dd
using Pkg

# ╔═╡ e20f9281-c5ed-4c00-b534-911da8718192
Pkg.activate("Project.toml")

# ╔═╡ 5e5247ab-db45-4f65-9ef0-10a01788c222
using CSV

# ╔═╡ 1a453080-90f0-4b0b-987e-ddf73c0d2853
using DataFrames

# ╔═╡ dbcf4272-6e7b-4ac7-8191-0ba742cfa07a
using PlutoUI

# ╔═╡ d475c4a0-89c0-4a18-a7be-a9f35f93d021
using Statistics

# ╔═╡ ef2bcbed-3bbd-4aa0-aec3-adf37a2ba218
using RDatasets

# ╔═╡ 3b329046-0289-456b-b07f-55487e04a9d9
using StatsBase

# ╔═╡ a74084dc-0d6d-441e-813b-9ec1be7d8983
using Plots

# ╔═╡ 9608118f-c2c5-4d04-9d6e-ff10cc4c1c88
using EvalMetrics

# ╔═╡ 83c2d364-b16a-11eb-0534-edbb1e4bd393
md"# Logistic Regression Model from scratch with Julia"

# ╔═╡ 919970b4-9577-4701-b5fc-d26de603e129
md"## Import libraries"

# ╔═╡ 46d374fc-86b0-4f9e-b772-74dea0b7825b
md"## load the dataset"

# ╔═╡ a2852b5d-04f5-4fbe-a149-059fec749fe1
hdf = CSV.read("data/heart-disease.csv", DataFrame)

# ╔═╡ 9d435f59-d2d7-4966-9515-99c9f3233ee5
md"## Quick exploration of the dataset"

# ╔═╡ 28a1851e-0a13-4169-ac9c-7ee8f09db2d9
md"### size and column names"

# ╔═╡ 35af15a6-1ad4-4e0a-8317-eec04d38b254
size(hdf)

# ╔═╡ b1512879-7f8b-4f2e-93d1-5fc0fc4d1fcc
with_terminal() do
	for col_name in names(hdf)
		println(col_name)
	end
end

# ╔═╡ 2365b92a-616c-432c-b839-17f8560d7287
first(hdf, 20)

# ╔═╡ 9d3fa681-f893-4942-9dbf-3b7906cc7c48
with_terminal() do
	describe(hdf[!,:male])
end

# ╔═╡ be3b1cff-bbf5-45da-b3b6-b11215b1b8e6
with_terminal() do
	describe(hdf[!,:age])
end

# ╔═╡ 31e3186c-59f3-4316-9216-db1e8e721ed0
with_terminal() do
	describe(hdf[!,:education])
end

# ╔═╡ 132d7d76-84f5-4fb8-a9bb-1695db5fab22
countmap(hdf[!,:education])

# ╔═╡ 570798be-c0e7-44f3-a7e1-7bb285b58e3f
with_terminal() do
	describe(hdf[!,:currentSmoker])
end

# ╔═╡ e53d803c-8d47-4be6-adaf-b002530bdbc7
with_terminal() do
	describe(hdf[!,:cigsPerDay])
end

# ╔═╡ 1d4f63f4-f0e5-4f8e-b25f-43223fd83ce8
countmap(hdf[!, :cigsPerDay])

# ╔═╡ b2dfd03a-4660-4fea-97ef-3221daeb5c6c
with_terminal() do
	describe(hdf[!,:BPMeds])
end

# ╔═╡ c8ae9d2d-d2ba-4d60-96dc-837a866fc5d0
countmap(hdf[!, :BPMeds])

# ╔═╡ babb204d-6336-40dc-b4d6-6627db467079
with_terminal() do
	describe(hdf[!,:prevalentStroke])
end

# ╔═╡ 5484887e-139c-45b5-904d-e6b68ce74038
with_terminal() do
	describe(hdf[!,:prevalentHyp])
end

# ╔═╡ 155ecd6f-6857-4a75-b9f3-b38ba7893084
with_terminal() do
	describe(hdf[!,:diabetes])
end

# ╔═╡ 405b09f4-48c0-4ce2-9c23-f3741559af76
with_terminal() do
	describe(hdf[!,:totChol])
end

# ╔═╡ 32d43966-f22c-4891-aa35-504d49708fdf
countmap(hdf[!, :totChol])

# ╔═╡ 91794280-0466-4a4d-be23-f405cfb40c6b
with_terminal() do
	describe(hdf[!,:sysBP])
end

# ╔═╡ 267733ae-0501-4368-bc8f-0a1916f668c7
with_terminal() do
	describe(hdf[!,:diaBP])
end

# ╔═╡ a0c66ef5-1599-4095-8ab1-8db5f5a2e3de
with_terminal() do
	describe(hdf[!,:BMI])
end

# ╔═╡ ed1f1c50-1a47-43e6-bd0e-9c616f700b8c
countmap(hdf[!, :BMI])

# ╔═╡ bed04079-3982-4591-8931-d702e12c0350
with_terminal() do
	describe(hdf[!,:heartRate])
end

# ╔═╡ bf787651-5e2d-4021-9baa-bda7c5829c7e
countmap(hdf[!, :heartRate])

# ╔═╡ b9ba4a2f-0578-4a7c-8aed-077b9f84e568
with_terminal() do
	describe(hdf[!,:glucose])
end

# ╔═╡ ba98c1a4-22fc-4d75-baa6-023f286c998a
countmap(hdf[!,:glucose])

# ╔═╡ cebb9a86-1fa4-48a0-97b5-6bb2e8dea253
with_terminal() do
	describe(hdf[!,:TenYearCHD])
end

# ╔═╡ fb2686b5-6fd3-45af-ba5a-9c33117de819
describe(hdf)

# ╔═╡ 026a0378-5253-4600-839e-2bbde3238c5a
countmap(hdf[!,:TenYearCHD])

# ╔═╡ 1346962b-386f-421f-b895-2a481e17f1b0
hdf2 = deepcopy(hdf)

# ╔═╡ fcbc11ee-652c-4ccc-a9f6-b9ffa1649cb8
size(hdf2)[2]

# ╔═╡ a3472ebe-c3af-42aa-9fff-2d58985a2687
education_vec = Vector{Int64}(undef, size(hdf)[1])

# ╔═╡ 0287c2a1-37f4-4f13-8294-5bd3dbee37f8
begin
	edu_str_vec = hdf2[!, :education]
	na_repl_edu = mode(filter(x -> x != "NA", edu_str_vec))
	for (s_ind, s_sample) in enumerate(edu_str_vec)
		if s_sample == "NA"
			education_vec[s_ind] = abs(parse(Int, na_repl_edu))
		else
			education_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ f5308e87-818c-4ebc-811d-59099309bc91
education_vec

# ╔═╡ 11714838-64ab-4e4e-bf11-f242ba63e67e
cigspd_vec = Vector{Int64}(undef, size(hdf)[1])

# ╔═╡ d810d4cb-173e-4ae6-aa4a-a98d4e35c421
begin
	cigs_str_vec = hdf2[!, :cigsPerDay]
	na_repl_cigs = mode(filter(x -> x != "NA", cigs_str_vec))
	for (s_ind, s_sample) in enumerate(cigs_str_vec)
		if s_sample == "NA"
			cigspd_vec[s_ind] = abs(parse(Int, na_repl_cigs))
		else
			cigspd_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ dde45cb6-bb8b-49a9-985a-9a42a652f581
cigspd_vec

# ╔═╡ f2b31df7-d491-422f-b129-8d38ad01503e
begin
	bp_meds_vec = Vector{Int64}(undef, size(hdf)[1])
	bpmeds_str_vec = hdf2[!, :BPMeds]
	na_repl_bpmeds = mode(filter(x -> x != "NA", bpmeds_str_vec))
	for (s_ind, s_sample) in enumerate(bpmeds_str_vec)
		if s_sample == "NA"
			bp_meds_vec[s_ind] = abs(parse(Int, na_repl_bpmeds))
		else
			bp_meds_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ 5f327e7c-17b8-4835-b3ff-acfafa177b44
bp_meds_vec

# ╔═╡ f1574f98-9d7b-465b-9b34-9866951a1d64
begin
	tot_chol_vec = Vector{Int64}(undef, size(hdf)[1])
	tot_chol_str_vec = hdf2[!, :totChol]
	na_repl_tot_chol = mode(filter(x -> x != "NA", tot_chol_str_vec))
	for (s_ind, s_sample) in enumerate(tot_chol_str_vec)
		if s_sample == "NA"
			tot_chol_vec[s_ind] = abs(parse(Int, na_repl_tot_chol))
		else
			tot_chol_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ 50aa56aa-6591-459b-99c4-b84dcfc1d172
tot_chol_vec

# ╔═╡ 01eb9dc1-6f96-40b5-b3bb-ec0ece5be06f
begin
	bmi_vec = Vector{Float64}(undef, size(hdf)[1])
	bmi_vec_str =  hdf2[!, :BMI]
	na_repl_val = mode(filter(x -> x != "NA",bmi_vec_str))
	for (s_ind, s_sample) in enumerate(bmi_vec_str)
		if s_sample == "NA"
			bmi_vec[s_ind] = abs(parse(Float64, na_repl_val))
		else
			bmi_vec[s_ind] = abs(parse(Float64, s_sample))
		end
	end
end

# ╔═╡ 24e33576-ff25-4c46-8302-676c47a96cf1
bmi_vec

# ╔═╡ 5464fc2d-45ef-4d68-a6bc-0b84de958067
begin
	heart_rate_vec = Vector{Int64}(undef, size(hdf)[1])
	heart_rate_str_vec = hdf2[!, :heartRate]
	na_repl_heart_rate = mode(filter(x -> x != "NA", heart_rate_str_vec))
	for (s_ind, s_sample) in enumerate(heart_rate_str_vec)
		if s_sample == "NA"
			heart_rate_vec[s_ind] = abs(parse(Int, na_repl_heart_rate))
		else
			heart_rate_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ dcad17cb-7375-475e-bc7b-5a9dd734b0cc
heart_rate_vec

# ╔═╡ 73f8d205-79a3-4429-9977-ebcae3a84026
begin
	glucose_vec = Vector{Int64}(undef, size(hdf)[1])
	glucose_str_vec = hdf2[!, :glucose]
	na_repl_glucose = mode(filter(x -> x != "NA", glucose_str_vec))
	for (s_ind, s_sample) in enumerate(glucose_str_vec)
		if s_sample == "NA"
			glucose_vec[s_ind] = abs(parse(Int, na_repl_glucose))
		else
			glucose_vec[s_ind] = abs(parse(Int, s_sample))
		end
	end
end

# ╔═╡ 2bec13ac-657d-4165-8a9a-402b6c0cca36
glucose_vec

# ╔═╡ 9a3f8efa-e3f3-4bc5-b3d6-164fe3eb2cdf
final_df = DataFrame(male=hdf2[!, :male], age=hdf2[!, :age], education=education_vec, current_smoker=hdf2[!, :currentSmoker], cigs_per_day=cigspd_vec, bp_meds=bp_meds_vec, prevalent_stroke=hdf2[!, :prevalentStroke], prevalent_hyp=hdf2[!, :prevalentHyp], diabetes=hdf2[!, :diabetes], tot_chol=tot_chol_vec, sys_bp=hdf2[!, :sysBP], dia_bp=hdf2[!, :diaBP], bmi=bmi_vec, heart_rate=heart_rate_vec, glucose=glucose_vec,ten_year_chd=hdf2[!, :TenYearCHD])

# ╔═╡ 8a64fea4-12c0-478a-a4d5-ebc93a6f189c
md"## Extract the matrices"

# ╔═╡ c671d7bf-5b76-41e7-a095-c9c4c7c0d5ce
X = final_df[:, 1:15]

# ╔═╡ 02e4a6ab-7550-4491-adff-c81c3ac911e0
X_mat = Matrix(X)

# ╔═╡ 2804b105-16e6-47d5-aad7-8610fac5aad3
Y = final_df[:, 16]

# ╔═╡ aa1d1285-abb7-45d9-a126-8514e15ec9d1
Y_vec = Vector(Y)

# ╔═╡ 57ef2d26-1c9a-47f7-90e7-a21bafbd6243
training_size = 0.7

# ╔═╡ 9e500c25-47ca-4c03-95e1-c2435003a3b4
all_data_size = size(final_df)[1]

# ╔═╡ ca645012-4e0e-4eb9-9e81-35ac30e78ce3
training_index = trunc(Int, training_size * all_data_size)

# ╔═╡ 28089ea0-62c2-4e56-8043-72e3ead2700e
X_mat_train = X_mat[1:training_index, :]

# ╔═╡ c93b1f71-672b-44d1-bbb7-2ac23333adf4
X_mat_test = X_mat[1+training_index:end,:]

# ╔═╡ 3253651e-def0-4fc5-87cd-8e2dfc550d88
Y_vec_train = Y_vec[1:training_index]

# ╔═╡ 4fe0aaa6-ed5d-4bda-b34b-77e8f6e8bd48
Y_vec_test = Y_vec[1+training_index:end]

# ╔═╡ 39d69849-d050-4062-b2ee-78d8d21581a4
function get_scaling_params(init_feature_mat)
	feat_mean = mean(init_feature_mat, dims=1)
	feat_dev = std(init_feature_mat, dims=1)
	return (feat_mean, feat_dev)
end

# ╔═╡ e72795c6-32ed-49d9-b550-98152b605aab
scaling_params = get_scaling_params(X_mat)

# ╔═╡ 62c23b07-2805-4c38-8b61-86375e316554
function scale_features(feature_mat, sc_params)
	scaled_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 9d59d2bb-9a08-49e1-af9b-0b6e8f354c43
scaled_training_features = scale_features(X_mat_train, scaling_params)

# ╔═╡ e4a334d4-06c9-4b4c-be01-30644ffa49ef
scaled_testing_features = scale_features(X_mat_test, scaling_params)

# ╔═╡ 967eac04-b992-46f1-b543-d209107efd1f
function sigmoid(z)
	1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ d94e7076-f6e1-4c85-a6b2-06b3f493f6bd
function get_cost(aug_features, outcome, weights, reg_param)
	sample_count = length(outcome)
	hypothesis = sigmoid(aug_features * weights)
		
	cost_part_1 = ((-outcome)' * log.(hypothesis))[1]
	cost_part_2 = ((1 .- outcome)' * log.(1 .- hypothesis))[1]
	
	lambda_regul = (reg_param/(2 * sample_count) * sum(weights[2:end] .^ 2))
	
	error = (1/sample_count) * (cost_part_1 - cost_part_2) + lambda_regul
	
	grad_error_all = ((1/sample_count) * (aug_features') * (hypothesis - outcome)) + ((1/sample_count) * (reg_param * weights))
	
	grad_error_all[1] = ((1/sample_count) * (aug_features[:,1])' * (hypothesis - outcome))[1]

	return(error, grad_error_all)
end

# ╔═╡ 4c99989f-360a-4d73-a569-9f7e03815dd2
function train_model(features, outcome, reg_param, alpha, max_iter)
	sample_count = length(outcome)
	aug_features = hcat(ones(sample_count,1), features)
	feature_count = size(aug_features)[2]
	weights = zeros(feature_count)
	errors = zeros(max_iter)
	
	for i in 1:max_iter
		error_and_grad_tp = get_cost(aug_features, outcome, weights, reg_param)
		errors[i] = error_and_grad_tp[1]
		weights = weights - (alpha * error_and_grad_tp[2])
	end
	return (errors, weights)
end

# ╔═╡ 8830ea1f-a9d4-4a47-854f-c5ffd2f92f92
tr_weights_errors = train_model(scaled_training_features, Y_vec_train, 0.001, 0.2, 4000)

# ╔═╡ 90c90234-cfdf-4f4e-a60c-ed4d0e4408fc
plot(tr_weights_errors[1],label="Cost",ylabel="Cost",xlabel="Number of Iteration", title="Cost Per Iteration")

# ╔═╡ 4973bb56-f299-46c0-bf2e-5822a036e564
function get_predictions(features, weights)
	total_entry_count = size(features)[1]
	aug_features = hcat(ones(total_entry_count, 1), features)
	preds = sigmoid(aug_features * weights)
	return preds
end

# ╔═╡ c2791de1-39bd-48e1-a850-4921ee4d0a34
function get_predicted_classes(preds, threshold)
	return preds .>= threshold
end

# ╔═╡ bfa7d9f8-5397-431b-b3e1-c40e329db7eb
tr_predicted_cls = get_predicted_classes(get_predictions(scaled_training_features, tr_weights_errors[2]), 0.5)

# ╔═╡ 9dfff527-b5d6-48db-81eb-c709240f8434
ts_predicted_cls = get_predicted_classes(get_predictions(scaled_testing_features, tr_weights_errors[2]), 0.5)

# ╔═╡ fecda30f-18c6-4df3-9722-422f946c01c7
train_conf_matrix = ConfusionMatrix(Y_vec_train, tr_predicted_cls)

# ╔═╡ ace86f6b-b936-46d5-85d2-94c723d981bb
test_conf_matrix = ConfusionMatrix(Y_vec_test, ts_predicted_cls)

# ╔═╡ 03be7a68-9b8c-4488-9e1e-1d526df1e314
accuracy(test_conf_matrix)

# ╔═╡ 5f9fb2f6-73cb-441e-a470-50b8ecbc6f0a
recall(test_conf_matrix)

# ╔═╡ 8dc26583-a993-437f-b912-d18f0980a9ee
precision(test_conf_matrix)

# ╔═╡ Cell order:
# ╠═83c2d364-b16a-11eb-0534-edbb1e4bd393
# ╠═919970b4-9577-4701-b5fc-d26de603e129
# ╠═d3bc4664-1270-4e21-aac6-e7a9a7d9f5dd
# ╠═e20f9281-c5ed-4c00-b534-911da8718192
# ╠═5e5247ab-db45-4f65-9ef0-10a01788c222
# ╠═1a453080-90f0-4b0b-987e-ddf73c0d2853
# ╠═dbcf4272-6e7b-4ac7-8191-0ba742cfa07a
# ╠═d475c4a0-89c0-4a18-a7be-a9f35f93d021
# ╠═ef2bcbed-3bbd-4aa0-aec3-adf37a2ba218
# ╠═3b329046-0289-456b-b07f-55487e04a9d9
# ╠═a74084dc-0d6d-441e-813b-9ec1be7d8983
# ╠═9608118f-c2c5-4d04-9d6e-ff10cc4c1c88
# ╠═46d374fc-86b0-4f9e-b772-74dea0b7825b
# ╠═a2852b5d-04f5-4fbe-a149-059fec749fe1
# ╠═9d435f59-d2d7-4966-9515-99c9f3233ee5
# ╠═28a1851e-0a13-4169-ac9c-7ee8f09db2d9
# ╠═35af15a6-1ad4-4e0a-8317-eec04d38b254
# ╠═b1512879-7f8b-4f2e-93d1-5fc0fc4d1fcc
# ╠═2365b92a-616c-432c-b839-17f8560d7287
# ╠═9d3fa681-f893-4942-9dbf-3b7906cc7c48
# ╠═be3b1cff-bbf5-45da-b3b6-b11215b1b8e6
# ╠═31e3186c-59f3-4316-9216-db1e8e721ed0
# ╠═132d7d76-84f5-4fb8-a9bb-1695db5fab22
# ╠═570798be-c0e7-44f3-a7e1-7bb285b58e3f
# ╠═e53d803c-8d47-4be6-adaf-b002530bdbc7
# ╠═1d4f63f4-f0e5-4f8e-b25f-43223fd83ce8
# ╠═b2dfd03a-4660-4fea-97ef-3221daeb5c6c
# ╠═c8ae9d2d-d2ba-4d60-96dc-837a866fc5d0
# ╠═babb204d-6336-40dc-b4d6-6627db467079
# ╠═5484887e-139c-45b5-904d-e6b68ce74038
# ╠═155ecd6f-6857-4a75-b9f3-b38ba7893084
# ╠═405b09f4-48c0-4ce2-9c23-f3741559af76
# ╠═32d43966-f22c-4891-aa35-504d49708fdf
# ╠═91794280-0466-4a4d-be23-f405cfb40c6b
# ╠═267733ae-0501-4368-bc8f-0a1916f668c7
# ╠═a0c66ef5-1599-4095-8ab1-8db5f5a2e3de
# ╠═ed1f1c50-1a47-43e6-bd0e-9c616f700b8c
# ╠═bed04079-3982-4591-8931-d702e12c0350
# ╠═bf787651-5e2d-4021-9baa-bda7c5829c7e
# ╠═b9ba4a2f-0578-4a7c-8aed-077b9f84e568
# ╠═ba98c1a4-22fc-4d75-baa6-023f286c998a
# ╠═cebb9a86-1fa4-48a0-97b5-6bb2e8dea253
# ╠═fb2686b5-6fd3-45af-ba5a-9c33117de819
# ╠═026a0378-5253-4600-839e-2bbde3238c5a
# ╠═1346962b-386f-421f-b895-2a481e17f1b0
# ╠═fcbc11ee-652c-4ccc-a9f6-b9ffa1649cb8
# ╠═a3472ebe-c3af-42aa-9fff-2d58985a2687
# ╠═0287c2a1-37f4-4f13-8294-5bd3dbee37f8
# ╠═f5308e87-818c-4ebc-811d-59099309bc91
# ╠═11714838-64ab-4e4e-bf11-f242ba63e67e
# ╠═d810d4cb-173e-4ae6-aa4a-a98d4e35c421
# ╠═dde45cb6-bb8b-49a9-985a-9a42a652f581
# ╠═f2b31df7-d491-422f-b129-8d38ad01503e
# ╠═5f327e7c-17b8-4835-b3ff-acfafa177b44
# ╠═f1574f98-9d7b-465b-9b34-9866951a1d64
# ╠═50aa56aa-6591-459b-99c4-b84dcfc1d172
# ╠═01eb9dc1-6f96-40b5-b3bb-ec0ece5be06f
# ╠═24e33576-ff25-4c46-8302-676c47a96cf1
# ╠═5464fc2d-45ef-4d68-a6bc-0b84de958067
# ╠═dcad17cb-7375-475e-bc7b-5a9dd734b0cc
# ╠═73f8d205-79a3-4429-9977-ebcae3a84026
# ╠═2bec13ac-657d-4165-8a9a-402b6c0cca36
# ╠═9a3f8efa-e3f3-4bc5-b3d6-164fe3eb2cdf
# ╠═8a64fea4-12c0-478a-a4d5-ebc93a6f189c
# ╠═c671d7bf-5b76-41e7-a095-c9c4c7c0d5ce
# ╠═02e4a6ab-7550-4491-adff-c81c3ac911e0
# ╠═2804b105-16e6-47d5-aad7-8610fac5aad3
# ╠═aa1d1285-abb7-45d9-a126-8514e15ec9d1
# ╠═57ef2d26-1c9a-47f7-90e7-a21bafbd6243
# ╠═9e500c25-47ca-4c03-95e1-c2435003a3b4
# ╠═ca645012-4e0e-4eb9-9e81-35ac30e78ce3
# ╠═28089ea0-62c2-4e56-8043-72e3ead2700e
# ╠═c93b1f71-672b-44d1-bbb7-2ac23333adf4
# ╠═3253651e-def0-4fc5-87cd-8e2dfc550d88
# ╠═4fe0aaa6-ed5d-4bda-b34b-77e8f6e8bd48
# ╠═39d69849-d050-4062-b2ee-78d8d21581a4
# ╠═e72795c6-32ed-49d9-b550-98152b605aab
# ╠═62c23b07-2805-4c38-8b61-86375e316554
# ╠═9d59d2bb-9a08-49e1-af9b-0b6e8f354c43
# ╠═e4a334d4-06c9-4b4c-be01-30644ffa49ef
# ╠═967eac04-b992-46f1-b543-d209107efd1f
# ╠═d94e7076-f6e1-4c85-a6b2-06b3f493f6bd
# ╠═4c99989f-360a-4d73-a569-9f7e03815dd2
# ╠═8830ea1f-a9d4-4a47-854f-c5ffd2f92f92
# ╠═90c90234-cfdf-4f4e-a60c-ed4d0e4408fc
# ╠═4973bb56-f299-46c0-bf2e-5822a036e564
# ╠═c2791de1-39bd-48e1-a850-4921ee4d0a34
# ╠═bfa7d9f8-5397-431b-b3e1-c40e329db7eb
# ╠═9dfff527-b5d6-48db-81eb-c709240f8434
# ╠═fecda30f-18c6-4df3-9722-422f946c01c7
# ╠═ace86f6b-b936-46d5-85d2-94c723d981bb
# ╠═03be7a68-9b8c-4488-9e1e-1d526df1e314
# ╠═5f9fb2f6-73cb-441e-a470-50b8ecbc6f0a
# ╠═8dc26583-a993-437f-b912-d18f0980a9ee
