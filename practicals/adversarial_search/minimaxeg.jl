### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 0b57d322-4c8c-4059-aea0-4c41a4b05911
using Pkg

# ╔═╡ 5f8aaead-1f9a-4061-94c6-e1af564ceac8
Pkg.activate("Project.toml")

# ╔═╡ 7a682968-acf1-11eb-16ed-c366f2578669
md"# Implementation of a MiniMax algorithm"

# ╔═╡ c402f710-857f-401b-a9c5-59464c0ae809
md"## define nodes"

# ╔═╡ 19cf8b06-0d6c-4820-a70e-dc83a874be42
abstract type AdversarialNode end

# ╔═╡ 48464c02-e946-4cdf-8439-637dea859019
struct TerminalNode <: AdversarialNode
	utility::Int64
end

# ╔═╡ bdbaa42e-1ede-4236-af19-4fd4e9cc179a
@enum PlayerAction PMIN PMAX

# ╔═╡ 227a2bf2-53f4-4a54-902b-54ae51dab58d
struct PlayerNode <: AdversarialNode
	action::PlayerAction
	nodes::Vector{Union{TerminalNode,PlayerNode}}
end

# ╔═╡ 41775c25-32d4-478d-9217-3fd4b1620859
struct Game
	initial::PlayerNode
end

# ╔═╡ 83a2eb37-78c7-4605-8ceb-e384ec6eb811
function evaluate_node(node::TerminalNode)
	return node.utility
end

# ╔═╡ dbedd6b0-848c-4c73-a6cf-60db04d03ac8
function evaluate_node(node::PlayerNode)
	eval_nodes = map(single_node -> evaluate_node(single_node), node.nodes)
	if node.action == PMIN
		return minimum(eval_nodes)
	else
		return maximum(eval_nodes)
	end
end

# ╔═╡ c4c30101-b582-4112-9815-377911e6c22a
function solve_game(game::Game)
	evaluate_node(game.initial)
end

# ╔═╡ f848625e-e216-449e-9190-b3dbf935d995
pn1 = PlayerNode(PMIN, [TerminalNode(3), TerminalNode(5), TerminalNode(10)]) 

# ╔═╡ 15014084-953b-4894-b4b4-71e9c2f39936
pn2 = PlayerNode(PMIN, [TerminalNode(2), TerminalNode(8), TerminalNode(19)])

# ╔═╡ 333accd7-e5f2-4e67-81bb-743fb7d09a58
pn3 = PlayerNode(PMIN, [TerminalNode(2), TerminalNode(7), TerminalNode(3)])

# ╔═╡ 21043b58-aa30-4346-b87e-191e793edf5d
pn4 = PlayerNode(PMAX, [pn1, pn2, pn3])

# ╔═╡ c92e025b-83c0-4443-8e08-e9f61506a45e
game = Game(pn4)

# ╔═╡ bebb3b8c-689f-49df-92f3-024c4f67e1bc
solve_game(game)

# ╔═╡ Cell order:
# ╠═7a682968-acf1-11eb-16ed-c366f2578669
# ╠═0b57d322-4c8c-4059-aea0-4c41a4b05911
# ╠═5f8aaead-1f9a-4061-94c6-e1af564ceac8
# ╠═c402f710-857f-401b-a9c5-59464c0ae809
# ╠═19cf8b06-0d6c-4820-a70e-dc83a874be42
# ╠═48464c02-e946-4cdf-8439-637dea859019
# ╠═bdbaa42e-1ede-4236-af19-4fd4e9cc179a
# ╠═227a2bf2-53f4-4a54-902b-54ae51dab58d
# ╠═41775c25-32d4-478d-9217-3fd4b1620859
# ╠═c4c30101-b582-4112-9815-377911e6c22a
# ╠═83a2eb37-78c7-4605-8ceb-e384ec6eb811
# ╠═dbedd6b0-848c-4c73-a6cf-60db04d03ac8
# ╠═f848625e-e216-449e-9190-b3dbf935d995
# ╠═15014084-953b-4894-b4b4-71e9c2f39936
# ╠═333accd7-e5f2-4e67-81bb-743fb7d09a58
# ╠═21043b58-aa30-4346-b87e-191e793edf5d
# ╠═c92e025b-83c0-4443-8e08-e9f61506a45e
# ╠═bebb3b8c-689f-49df-92f3-024c4f67e1bc
