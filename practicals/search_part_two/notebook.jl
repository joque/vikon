### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 7a38bd1c-ac07-11eb-1997-15d754922b16
using Pkg

# ╔═╡ b1d3e2b6-1091-439e-90d8-441cff0e44fa
Pkg.activate("Project.toml")

# ╔═╡ 262da650-5efb-402a-a087-0fa24eb8d92d
md"Note that the implementation below has been tailored for a four-queen board"

# ╔═╡ df8addcb-6616-4b61-a2bc-c6afada2d84b
md"## Fixing each queen in a column"

# ╔═╡ 9eb8537f-13d8-4e3a-bb53-94194c3c4aba
col_mapping = Dict(:q1 => 1, :q2 => 2, :q3 => 3, :q4 => 4)

# ╔═╡ 83e0e8bc-819a-4806-9bef-54bbc4b0eb07
md"## Define a struct to represent a configuration or state"

# ╔═╡ 03f63cc3-ff3a-4193-bb64-71cd9de94732
md"here we control the possible values for a row in a column"

# ╔═╡ 2db15e82-343c-4c66-8d0d-3fabca021bcb
struct QueenState
	configuration::Dict{Symbol, Int64}
	function QueenState(row_vec::Vector{Int64})
		for single_row in row_vec
			if ((single_row < 1) || (single_row > 4))
				return error("row index out of bounds")
			end
		end
		new(Dict(:q1 => row_vec[1], :q2 => row_vec[2], :q3 => row_vec[3],:q4 => row_vec[4]))
	end
end
			

# ╔═╡ c358a436-9cb9-4dd8-97ab-0637ba1ed70e
init_config = QueenState([3,2,3,2])

# ╔═╡ 667d638f-32de-4999-bed3-51449ae9611c
function convert_state_to_vec(state::QueenState)
	cur_configuration = state.configuration
	return [cur_configuration[:q1], cur_configuration[:q2], cur_configuration[:q3], cur_configuration[:q4]]
end

# ╔═╡ a279ca42-d6c1-421b-b1f5-92fcf6fee94d
convert_state_to_vec(init_config)

# ╔═╡ 7f6a7159-ee43-40fc-bd8f-deccb294b6bf
md"## count the number of row and diagonal attacks between two queens"

# ╔═╡ 5d9b343f-a414-43ef-9864-dc5779e0ba6f
function count_attacks(state::QueenState, chck_tuple, board_mapping)
	if state.configuration[chck_tuple[1]] == state.configuration[chck_tuple[2]]
		#row attack
		return 1
	else
		#check for diagonal attack
		col_dist = abs(board_mapping[chck_tuple[1]] - board_mapping[chck_tuple[2]])
		row_dist = abs(state.configuration[chck_tuple[1]] - state.configuration[chck_tuple[2]])
		return col_dist == row_dist ? 1 : 0
	end
end

# ╔═╡ 958757da-5dfe-4dcc-809e-4d09d328c9cd
md"## evaluate a state"

# ╔═╡ 702771d2-4c75-44cd-881e-d085b716e8b9
function evaluate_state(state::QueenState, board_mapping)
	check_vec = [(:q1,:q2), (:q1,:q3), (:q1,:q4), (:q2,:q3), (:q2,:q4),(:q3,:q4)]
	total_count = 0
	for single_tpl in check_vec
		total_count += count_attacks(state, single_tpl, board_mapping)
	end
	return total_count
end

# ╔═╡ 603ad4a4-1771-4403-9dd3-b09e601b0d86
evaluate_state(init_config, col_mapping)

# ╔═╡ bf768793-e06a-4932-b8cc-a4b3ab117362
evaluate_state(QueenState([3,1,3,2]), col_mapping)

# ╔═╡ 480fbbf4-4e6c-440c-9d9e-bb51881e2e68
md"## generate all twelve neighbours for a given state"

# ╔═╡ ab6c2972-05df-44f9-be70-8d1adea94f0e
function generate_neighbours(state::QueenState)
	all_neighbours = []
	rows = convert_state_to_vec(state)
	for i in 1:4
		for j in 1:4
			cur_vec = deepcopy(rows)
			cur_vec[i] = j
			if !(cur_vec == rows)
				push!(all_neighbours, QueenState(cur_vec))
			end
		end
	end
	return all_neighbours
end

# ╔═╡ 984bc5a6-12d6-4347-9512-56d22d4e1fef
function find_best_successor(state::QueenState, board_mapping)
	cur_neighbours = generate_neighbours(state)
	min_pos = findmin(map(st -> evaluate_state(st, board_mapping), cur_neighbours))
	return (cur_neighbours[min_pos[2]], min_pos[1])
end

# ╔═╡ 78ed28bb-78be-4af2-9141-d18d51219732
find_best_successor(init_config, col_mapping)

# ╔═╡ bd39f348-96e7-4060-b0c9-5b60288c917e
md"## vanilla hill climbing algorithm"

# ╔═╡ 3c9a8549-4bbc-4e49-a660-695fb342c3d9
function local_search_queen(init_state::QueenState, board_mapping)
	current_state = init_state
	while true
		bn_tuple = find_best_successor(current_state, board_mapping)
		if bn_tuple[2] > evaluate_state(current_state, board_mapping)
			return current_state
		end
		current_state = bn_tuple[1]
	end
end

# ╔═╡ 404fee42-92d5-41d4-8f46-203e02a678b4
local_search_queen(init_config, col_mapping)

# ╔═╡ Cell order:
# ╠═7a38bd1c-ac07-11eb-1997-15d754922b16
# ╠═b1d3e2b6-1091-439e-90d8-441cff0e44fa
# ╟─262da650-5efb-402a-a087-0fa24eb8d92d
# ╠═df8addcb-6616-4b61-a2bc-c6afada2d84b
# ╠═9eb8537f-13d8-4e3a-bb53-94194c3c4aba
# ╠═83e0e8bc-819a-4806-9bef-54bbc4b0eb07
# ╠═03f63cc3-ff3a-4193-bb64-71cd9de94732
# ╠═2db15e82-343c-4c66-8d0d-3fabca021bcb
# ╠═c358a436-9cb9-4dd8-97ab-0637ba1ed70e
# ╠═667d638f-32de-4999-bed3-51449ae9611c
# ╠═a279ca42-d6c1-421b-b1f5-92fcf6fee94d
# ╠═7f6a7159-ee43-40fc-bd8f-deccb294b6bf
# ╠═5d9b343f-a414-43ef-9864-dc5779e0ba6f
# ╠═958757da-5dfe-4dcc-809e-4d09d328c9cd
# ╠═702771d2-4c75-44cd-881e-d085b716e8b9
# ╠═603ad4a4-1771-4403-9dd3-b09e601b0d86
# ╠═bf768793-e06a-4932-b8cc-a4b3ab117362
# ╠═480fbbf4-4e6c-440c-9d9e-bb51881e2e68
# ╠═ab6c2972-05df-44f9-be70-8d1adea94f0e
# ╠═984bc5a6-12d6-4347-9512-56d22d4e1fef
# ╠═78ed28bb-78be-4af2-9141-d18d51219732
# ╠═bd39f348-96e7-4060-b0c9-5b60288c917e
# ╠═3c9a8549-4bbc-4e49-a660-695fb342c3d9
# ╠═404fee42-92d5-41d4-8f46-203e02a678b4
