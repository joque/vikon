### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 7834f657-ce88-490e-834d-9d5bbb4dd75a
using Pkg

# ╔═╡ 8317f3da-2d1b-413d-8a3f-4913df9366a2
Pkg.activate("Project.toml")

# ╔═╡ 15d74744-c109-46b2-a8fc-08a9babc65a9
using PlutoUI

# ╔═╡ b324dbaa-f28e-401f-8758-3447b7391f4b
using DataStructures

# ╔═╡ 31c09ff7-dbc3-4f10-b9de-a8d4c9d8dad6
md"# Lab2"

# ╔═╡ 45bf7e01-8dd4-4abd-ae46-d608b731ee9d
md"## State definitions "

# ╔═╡ da11a750-a1d8-11eb-195a-dfc44f9a6052
struct State
	name::String
	position::Int64
	dirt_in_squares::Vector{Bool}
end

# ╔═╡ c63693f6-7df0-487e-b1bb-1227c373ab4c
md"## Action Definition "

# ╔═╡ 19830334-aefb-4cb7-a618-d2d5eea0c8b8
struct Action
	name::String
	cost::Int64
end

# ╔═╡ e6b83960-4f83-4c92-bff0-0a1d791442f9
md"### Assigning actions to squares"

# ╔═╡ 70243b7d-7433-4f14-adf0-066957d4e84a
A1 = Action("Left", 1)

# ╔═╡ aef3c411-aeb3-4e17-88aa-e96b742c0d57
A2 = Action("Right", 2)

# ╔═╡ 273b8f95-73f8-495d-9903-c7128265a581
A3 = Action("Pick_Up", 3)

# ╔═╡ 2fc3f013-740a-47fd-91fd-c67d2fbf96fd
md"### Assigning the states to the squares"

# ╔═╡ bfe89c9d-0ae7-4de1-ba90-d3541ef7de68
S1 = State("State one", 1, [true, true, true])

# ╔═╡ 54232250-d658-4723-acf6-de448d48d9f1
S2 = State("State two", 2, [true, true, true])

# ╔═╡ c02bcb30-8bf6-4d09-af28-c3f7f50c8aa1
S3 = State("State three", 3, [true, true, true])

# ╔═╡ 7e4b95da-99a6-4251-b43c-b98292478b00
S4 = State("State four", 1, [false, true, true])

# ╔═╡ 909e1b23-6953-4957-bcdb-84dfb993f998
S5 = State("State five", 2, [false, true, true])

# ╔═╡ e3afb090-3ca9-4771-9398-d2bbf1384a79
S6 = State("State six", 3, [false, true, true])

# ╔═╡ 1ed49515-e7fb-461c-a611-e697448de3af
S7 = State("State seven", 3, [false, true, false])

# ╔═╡ 638f3ac7-c642-48dc-94a0-2fc26a4ee633
md"## Create transition model"

# ╔═╡ 7c7d903a-0591-4e92-9fcd-adb38db6a089
Transition_Mod = Dict()

# ╔═╡ 2ccea3b1-eac0-426a-ab64-4f340de01d75
md"### Add a mapping/push!  to the transition Model"

# ╔═╡ 9fc91dde-70a1-4d40-aa63-d8f8fd719b59
push!(Transition_Mod, S1 => [(A2, S2), (A1, S1), (A3, S4)])

# ╔═╡ d2d4257d-48ad-4ab3-997a-66bd6e466779
push!(Transition_Mod, S2 => [(A2, S3), (A1, S1)])

# ╔═╡ 8290be9a-60cf-40b5-814b-86019ef78337
push!(Transition_Mod, S3 => [(A2, S3), (A1, S2)])

# ╔═╡ c6c84321-8e0e-4ed8-afef-d3867fdd8a25
push!(Transition_Mod, S4 => [(A1, S4), (A2, S5)])

# ╔═╡ 025a91e1-9b01-4289-bd59-ea11dde47285
push!(Transition_Mod, S5 => [(A1, S4), (A2, S6)])

# ╔═╡ 21646d90-7d03-4f4d-9495-0077c4c797e0
push!(Transition_Mod, S6 => [(A1, S5), (A2, S6), (A3, S7)])

# ╔═╡ c28e8824-7ce7-47bd-a272-047bf72bfc60
Transition_Mod

# ╔═╡ 4f62ce72-2aa7-4feb-9141-71ce26da9398
md"# Write the search strategy and then traverse the entire state space"

# ╔═╡ e0e84746-f05c-4eb2-bce9-cd3315217e66
function create_result(trans_model, ancestors, initial_state, goal_state)
	#move backward from the goal state to the inital state
	#ends when you find the initial state
	result = []
	explorer = goal_state
	
	while !(explorer == initial_state)
		current_state_ancestor = ancestors[explorer]
		related_transitions = trans_model[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	
	return result
end

# ╔═╡ fb854a67-cb3c-4a49-93c0-4bc3735ca94d
function bfs_search(initial_state, transition_model, goal_states)
	result = []
	all_candidates = Queue{State}()
	explored = []
	ancestors = Dict{State, State}()
	first_state = true
	enqueue!(all_candidates, initial_state)
	parent = initial_state
	
	while true
		if isempty(all_candidates)
			return []
		else
			current_state = dequeue!(all_candidates)
						
			#proceed with handling the current state
			push!(explored, current_state)
			candidates = transition_model[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(ancestors, single_candidate[2] => current_state)
					if (single_candidate[2] in goal_states)
						return create_result(transition_model, ancestors, initial_state, single_candidate[2])
					else
						enqueue!(all_candidates, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ eb78d883-1d42-424e-9832-45285bcd698d
function all_search(initial_state, transition_dict, is_goal, all_candidates, add_candidate, remove_candidate)
	explored = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initial_state, 0)
	parent = initial_state
	
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
						
			#proceed with handling the current state
			push!(explored, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return create_result(transition_dict, ancestors, initial_state, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates, single_candidate[2], single_candidate[1].cost)
					end
				end
			end
		end
	end
end

# ╔═╡ ca0fdbf0-c713-44b1-9d65-63344d8d5226
function goal_test(current_state::State)
	return ! (current_state.dirt_in_squares[1] || current_state.dirt_in_squares[3])
end

# ╔═╡ 8ca970bd-acdc-4127-8df1-b4109a0d7788
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
	enqueue!(queue, state)
	return queue
end

# ╔═╡ 86efd05e-b7ef-4860-aa90-4c03c03424db
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
	push!(stack, state)
	return stack
end

# ╔═╡ 578612ab-4b33-45f3-b0cf-9078de3a5416
function remove_from_queue(queue::Queue{State})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ e79dff44-7960-4867-be61-e273d4f4efd7
function remove_from_stack(stack::Stack{State})
	removed = pop!(stack)
	return (removed, stack)
end

# ╔═╡ 6adeeabf-e788-4361-a31d-88510af97906
bfs_search(S1, Transition_Mod, [S7])

# ╔═╡ 1ef391e4-97a9-4f88-ba7e-fd7835254e33
md"### Generic call for breadth-first search"

# ╔═╡ 88e13897-7ac7-48cc-943a-2d725b1bdf90
all_search(S1, Transition_Mod, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ dc2fee50-5fe3-442d-869d-b20f59056ff3
md"### Generic call Depth-first search"

# ╔═╡ e99ad569-b6d9-49dd-9684-c23c28a7bdc2
all_search(S1, Transition_Mod, goal_test, Stack{State}(), add_to_stack, remove_from_stack)

# ╔═╡ bef4912c-e6a7-4a93-8b25-39d31867b367
function add_to_pqueue_ucs(queue::PriorityQueue{State, Int64}, state::State, cost::Int64)
	enqueue!(queue, state, cost)
	return queue
end

# ╔═╡ ed8a53fe-eaa5-4480-8083-710623083671
function remove_from_pqueue_ucs(queue::PriorityQueue{State, Int64})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ 42fc0a0b-d818-42fb-b163-5b7de2afb86f
md"### Generic call uniform cost search"

# ╔═╡ 46bb9efb-35a6-4141-9dd6-eedbba55f1c0
all_search(S1, Transition_Mod, goal_test, PriorityQueue{State, Int64}(Base.Order.Reverse), add_to_pqueue_ucs, remove_from_pqueue_ucs)

# ╔═╡ Cell order:
# ╠═31c09ff7-dbc3-4f10-b9de-a8d4c9d8dad6
# ╠═7834f657-ce88-490e-834d-9d5bbb4dd75a
# ╠═8317f3da-2d1b-413d-8a3f-4913df9366a2
# ╠═15d74744-c109-46b2-a8fc-08a9babc65a9
# ╠═b324dbaa-f28e-401f-8758-3447b7391f4b
# ╠═45bf7e01-8dd4-4abd-ae46-d608b731ee9d
# ╠═da11a750-a1d8-11eb-195a-dfc44f9a6052
# ╠═c63693f6-7df0-487e-b1bb-1227c373ab4c
# ╠═19830334-aefb-4cb7-a618-d2d5eea0c8b8
# ╟─e6b83960-4f83-4c92-bff0-0a1d791442f9
# ╠═70243b7d-7433-4f14-adf0-066957d4e84a
# ╠═aef3c411-aeb3-4e17-88aa-e96b742c0d57
# ╠═273b8f95-73f8-495d-9903-c7128265a581
# ╟─2fc3f013-740a-47fd-91fd-c67d2fbf96fd
# ╠═bfe89c9d-0ae7-4de1-ba90-d3541ef7de68
# ╠═54232250-d658-4723-acf6-de448d48d9f1
# ╠═c02bcb30-8bf6-4d09-af28-c3f7f50c8aa1
# ╠═7e4b95da-99a6-4251-b43c-b98292478b00
# ╠═909e1b23-6953-4957-bcdb-84dfb993f998
# ╠═e3afb090-3ca9-4771-9398-d2bbf1384a79
# ╠═1ed49515-e7fb-461c-a611-e697448de3af
# ╠═638f3ac7-c642-48dc-94a0-2fc26a4ee633
# ╠═7c7d903a-0591-4e92-9fcd-adb38db6a089
# ╠═2ccea3b1-eac0-426a-ab64-4f340de01d75
# ╠═9fc91dde-70a1-4d40-aa63-d8f8fd719b59
# ╠═d2d4257d-48ad-4ab3-997a-66bd6e466779
# ╠═8290be9a-60cf-40b5-814b-86019ef78337
# ╠═c6c84321-8e0e-4ed8-afef-d3867fdd8a25
# ╠═025a91e1-9b01-4289-bd59-ea11dde47285
# ╠═21646d90-7d03-4f4d-9495-0077c4c797e0
# ╠═c28e8824-7ce7-47bd-a272-047bf72bfc60
# ╠═4f62ce72-2aa7-4feb-9141-71ce26da9398
# ╠═e0e84746-f05c-4eb2-bce9-cd3315217e66
# ╠═fb854a67-cb3c-4a49-93c0-4bc3735ca94d
# ╠═eb78d883-1d42-424e-9832-45285bcd698d
# ╠═ca0fdbf0-c713-44b1-9d65-63344d8d5226
# ╠═8ca970bd-acdc-4127-8df1-b4109a0d7788
# ╠═86efd05e-b7ef-4860-aa90-4c03c03424db
# ╠═578612ab-4b33-45f3-b0cf-9078de3a5416
# ╠═e79dff44-7960-4867-be61-e273d4f4efd7
# ╠═6adeeabf-e788-4361-a31d-88510af97906
# ╟─1ef391e4-97a9-4f88-ba7e-fd7835254e33
# ╠═88e13897-7ac7-48cc-943a-2d725b1bdf90
# ╟─dc2fee50-5fe3-442d-869d-b20f59056ff3
# ╠═e99ad569-b6d9-49dd-9684-c23c28a7bdc2
# ╠═bef4912c-e6a7-4a93-8b25-39d31867b367
# ╠═ed8a53fe-eaa5-4480-8083-710623083671
# ╠═42fc0a0b-d818-42fb-b163-5b7de2afb86f
# ╠═46bb9efb-35a6-4141-9dd6-eedbba55f1c0
