### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 6ae08ff8-8e24-11eb-1828-557ca18a7c48
using Pkg

# ╔═╡ a5d20f6a-8e24-11eb-0420-abd3da684234
using PlutoUI

# ╔═╡ a7be5c6e-91e9-11eb-02d0-11a3b7351831
using CSV, DataFrames, Missings, Dates, VegaLite, VegaDatasets

# ╔═╡ ad7405de-8e24-11eb-3c94-81729ec0fd49
md"# General Introduction to Julia"

# ╔═╡ cc5341cc-8e24-11eb-0957-cb9ee5487a0a
md"""## This tutorial will cover the following
+ Basics
+ Data Structures
+ Control Flow
+ Functions
+ classes"""

# ╔═╡ c1424fbc-8e29-11eb-06fe-5d2e126a4146
md"## Packages"

# ╔═╡ 9d618c52-8e24-11eb-3220-15634c4d0bd4
Pkg.activate("Project.toml")

# ╔═╡ 8484095a-8e25-11eb-260a-1f64a4687fb3
md"## Basics"

# ╔═╡ 12e57ce0-8e25-11eb-273c-77acf22a2d1c
var1 = 5546

# ╔═╡ 1e885c34-8e25-11eb-072e-fd6789df79b0
with_terminal() do
	println(var1)
end

# ╔═╡ 2d9d3adc-8e25-11eb-29b1-6f195ff14810
# can do string interpolation
with_terminal() do
	println("The value in var1 is $var1")
end

# ╔═╡ 598113bc-8e25-11eb-1d6a-3591af93c3bc
begin
	name = "Tommy"
	with_terminal() do
		println("Hi there! $name")
	end
end

# ╔═╡ 7d69889a-8e25-11eb-3a20-41f681c71f3e
md"## Data structures"

# ╔═╡ 9803db88-8e25-11eb-199c-376f7dbe5544
md"### Dictionary -- mutable index-less data structures"

# ╔═╡ 68c30dc0-8e26-11eb-1f9a-3b9bedcf6f6b
student_groups = Dict("first" => "Group one", "second" => "Group two", "third" => "Group three", "fourth" => "Group four")

# ╔═╡ a2f4fc88-8e26-11eb-12ee-795cf9dd0f62
student_groups["third"]

# ╔═╡ b445e97a-8e26-11eb-1ec4-c32296420cfc
student_groups["first"]

# ╔═╡ bd248e66-8e26-11eb-06bf-591ef474082a
student_groups["first"] = "Initial Group"

# ╔═╡ cf345a64-8e26-11eb-2682-31d9502366a2
student_groups["fifth"] = "Group five"

# ╔═╡ e17c77d8-8e26-11eb-3cb6-cdbd8dfff70c
pop!(student_groups, "fourth")

# ╔═╡ f111c7fc-8e26-11eb-0ac9-b125c176914f
student_groups

# ╔═╡ f47ffe72-8e26-11eb-10fc-1790e4596a5e
md"### Tuple"

# ╔═╡ fb9897fa-8e26-11eb-3100-4f335b724a16
friends = ("Kent", "Tommy", "Bob", "Roger")

# ╔═╡ 1ddbd5b6-8e27-11eb-1c64-93ca081a4f47
friends[2]

# ╔═╡ 2c8f9c1e-8e27-11eb-2f64-8f0109ea8eae
friends[2] = "Sailly"

# ╔═╡ 35d289f8-8e27-11eb-2c87-4d009d286718
push!(friends, "Genevieve")

# ╔═╡ 502bf4d8-8e27-11eb-2025-0d0e767e4006
pop!(friends)

# ╔═╡ 5e65599a-8e27-11eb-3564-5941188c997a
md"### Array -- mutable multi-dimensional collections"

# ╔═╡ 6e648f46-8e27-11eb-38c8-2dfdb8ba4e1d
vals = [1,2,3,4,5,6,7]

# ╔═╡ 6ea945fe-8e28-11eb-163f-c505b0b9d01b
vals[1]

# ╔═╡ 752f2272-8e28-11eb-07c6-8f161017ec92
vals2 = rand(1:10, 5)

# ╔═╡ 89b4c314-8e28-11eb-265a-35a7ce3fee8d
vals2

# ╔═╡ 908835f4-8e28-11eb-1cb3-7d3c7792fb9e
vals[2:5]

# ╔═╡ e544f126-8e29-11eb-1134-6b125e36695a
vals3 = rand(Int64, 5, 5)

# ╔═╡ 283a80ea-8e2a-11eb-3285-5184f4b8549d
vals3[2:4,1:3]

# ╔═╡ 3ebdf48c-8e2a-11eb-32c9-afb5b723a55c
vals4 = rand(1:100, 5,5)

# ╔═╡ 5a71dcf4-8e2a-11eb-117d-c3f81c0ca92b
size(vals4)

# ╔═╡ 60d4f872-8e2a-11eb-3546-d9171d554434
size(vals4, 1)

# ╔═╡ e6b6966c-8e2a-11eb-09d4-5b8fbf3d15de
md"## Control flow"

# ╔═╡ f0d752f8-8e2a-11eb-2889-57472543dbf8
md"### Loops"

# ╔═╡ f6534d7c-8e2a-11eb-291a-2d785b7ab468
with_terminal() do
	counter = 1
	while counter <= length(vals)
		println("The element in vals at index $counter is $(vals[counter])")
		counter += 1
	end
end

# ╔═╡ 65d50284-8e2c-11eb-3e74-9948df906bae
with_terminal() do
	counter_row = 1
	counter_col = 1
	while counter_row <= 5
		while counter_col <= 5
			println("the element in matrix vals4 at row $counter_row and column $counter_col is $(vals4[counter_row, counter_col])")
			counter_col += 1
		end
		counter_row += 1
		counter_col = 1
		println("new row...")
	end
end

# ╔═╡ 318d549e-8e2d-11eb-1d73-d1488200d929
with_terminal() do
	for ind_i in 1:5, ind_j in 1:5
		println("the content of the vals4 matrix at ($ind_i, $ind_j) is $(vals4[ind_i, ind_j])")
	end
end

# ╔═╡ 9af3d034-8e2d-11eb-2689-af34d9c8e2c2
with_terminal() do
	if vals4[3,4] > 1
		println("the element at position (3,4) is greater than 1")
	elseif vals4[3,4] < 1
		println("the element at position (3,4) is less than 1")
	else
		println("the element at position (3,4) is  1")
	end
end

# ╔═╡ 33a960be-8e2e-11eb-356c-a3acc07cd298
with_terminal() do
	vals4[4,2] > 30 ? println("(4,2) is higher than 30") : println("(4,2) is less than 30")
end

# ╔═╡ add792b6-8e2e-11eb-05d1-55c62c2d304b
md"## Functions"

# ╔═╡ bc7e0156-8e2e-11eb-2ff0-1b2541453344
function greetings1(name)
	first_part = "Hello "
	return first_part * name
end

# ╔═╡ 803a09e6-8e2f-11eb-253b-dd7b881ab1f1
with_terminal() do
	println(greetings1("Paul"))
end

# ╔═╡ ade3a26c-8e2f-11eb-278c-ef7f61b29b69
greetings2(name) = "hi there! " * name

# ╔═╡ d2b61cd2-8e2f-11eb-2f58-d948ddfc44b5
with_terminal() do
	name = "Victor"
	println(greetings2(name))
end

# ╔═╡ 0156ec06-8e30-11eb-30b6-630128d6bf1e
greetings3 = name -> "Yo! " * name

# ╔═╡ 14b4c110-8e30-11eb-0348-35cc6b6de5f0
with_terminal() do
	println(greetings3("Samuel"))
end

# ╔═╡ 27b308a0-8e30-11eb-3594-6fb25629dc0d
muller = x -> x ^ 2

# ╔═╡ 76848f60-8e30-11eb-34d7-5b31582a1018
muller(3)

# ╔═╡ 7d8780c2-8e30-11eb-29bb-59e96aff1864
muller(vals4)

# ╔═╡ 81dcd4a8-8e30-11eb-2117-b997a5862cc7
muller(vals)

# ╔═╡ 8d61421e-8e30-11eb-00f8-a1787b433f6f
muller.(vals4)

# ╔═╡ 93927ee6-8e30-11eb-0ae3-6de9bceede4d
muller.(vals)

# ╔═╡ db729c96-8e30-11eb-2a29-3f18522feaa6
md"## Classes"

# ╔═╡ e21b782e-8e30-11eb-1f58-f7891f730b67
struct Student
	name::String
	year::Int64
	subjects::Array{String, 1}
end

# ╔═╡ 34cb49fa-8e31-11eb-252e-53fe2bc8048c
st_name(st::Student) = st.name

# ╔═╡ 099e5d02-8e32-11eb-2363-d3a94a868c54
year_of_study(st::Student) = st.year

# ╔═╡ 567c3458-8e32-11eb-1a87-71e4b2104382
#subject(st::Student, sind) = st.subjects[sind]
function subject(st::Student, sind)
	if sind <= length(st.subjects)
		return st.subjects[sind]
	else
		return "No ubject at index $sind"
	end
end

# ╔═╡ 1cb987f4-8e32-11eb-0748-89445972edfb
marc = Student("Marc Angelo", 3, ["AI", "Programming", "DSP"])

# ╔═╡ 409ba742-8e32-11eb-043c-b755c332cf01
st_name(marc)

# ╔═╡ 49165c00-8e32-11eb-17e1-a321d7e0fc1f
year_of_study(marc)

# ╔═╡ 529996de-8e32-11eb-17b7-4d9923c4405b
subject(marc, 3)

# ╔═╡ ff402106-8e31-11eb-0623-4bcd0f7e9c7d
subject(marc, 4)

# ╔═╡ 7f384348-8e32-11eb-33c5-3d8c444f1cec
md"## Back to packages"

# ╔═╡ 1fb2fc88-91ed-11eb-3b45-f57c25dcb0d2
df1 = DataFrame(CSV.File("data/nam-stats.csv"))

# ╔═╡ 5c57526a-91ed-11eb-3361-19c0cd550944
begin
DataFrame(CSV.File("data/nam-stats.csv")) |>
@vlplot(
		:line,
		transform=[{filter="datum.level === 'national'"}],
		x="date:t", 
		y=:confirmed
)
end

# ╔═╡ Cell order:
# ╟─ad7405de-8e24-11eb-3c94-81729ec0fd49
# ╟─cc5341cc-8e24-11eb-0957-cb9ee5487a0a
# ╟─c1424fbc-8e29-11eb-06fe-5d2e126a4146
# ╠═6ae08ff8-8e24-11eb-1828-557ca18a7c48
# ╠═9d618c52-8e24-11eb-3220-15634c4d0bd4
# ╠═a5d20f6a-8e24-11eb-0420-abd3da684234
# ╟─8484095a-8e25-11eb-260a-1f64a4687fb3
# ╠═12e57ce0-8e25-11eb-273c-77acf22a2d1c
# ╠═1e885c34-8e25-11eb-072e-fd6789df79b0
# ╠═2d9d3adc-8e25-11eb-29b1-6f195ff14810
# ╠═598113bc-8e25-11eb-1d6a-3591af93c3bc
# ╟─7d69889a-8e25-11eb-3a20-41f681c71f3e
# ╟─9803db88-8e25-11eb-199c-376f7dbe5544
# ╠═68c30dc0-8e26-11eb-1f9a-3b9bedcf6f6b
# ╠═a2f4fc88-8e26-11eb-12ee-795cf9dd0f62
# ╠═b445e97a-8e26-11eb-1ec4-c32296420cfc
# ╠═bd248e66-8e26-11eb-06bf-591ef474082a
# ╠═cf345a64-8e26-11eb-2682-31d9502366a2
# ╠═e17c77d8-8e26-11eb-3cb6-cdbd8dfff70c
# ╠═f111c7fc-8e26-11eb-0ac9-b125c176914f
# ╟─f47ffe72-8e26-11eb-10fc-1790e4596a5e
# ╠═fb9897fa-8e26-11eb-3100-4f335b724a16
# ╠═1ddbd5b6-8e27-11eb-1c64-93ca081a4f47
# ╠═2c8f9c1e-8e27-11eb-2f64-8f0109ea8eae
# ╠═35d289f8-8e27-11eb-2c87-4d009d286718
# ╠═502bf4d8-8e27-11eb-2025-0d0e767e4006
# ╟─5e65599a-8e27-11eb-3564-5941188c997a
# ╠═6e648f46-8e27-11eb-38c8-2dfdb8ba4e1d
# ╠═6ea945fe-8e28-11eb-163f-c505b0b9d01b
# ╠═752f2272-8e28-11eb-07c6-8f161017ec92
# ╠═89b4c314-8e28-11eb-265a-35a7ce3fee8d
# ╠═908835f4-8e28-11eb-1cb3-7d3c7792fb9e
# ╠═e544f126-8e29-11eb-1134-6b125e36695a
# ╠═283a80ea-8e2a-11eb-3285-5184f4b8549d
# ╠═3ebdf48c-8e2a-11eb-32c9-afb5b723a55c
# ╠═5a71dcf4-8e2a-11eb-117d-c3f81c0ca92b
# ╠═60d4f872-8e2a-11eb-3546-d9171d554434
# ╟─e6b6966c-8e2a-11eb-09d4-5b8fbf3d15de
# ╟─f0d752f8-8e2a-11eb-2889-57472543dbf8
# ╠═f6534d7c-8e2a-11eb-291a-2d785b7ab468
# ╠═65d50284-8e2c-11eb-3e74-9948df906bae
# ╠═318d549e-8e2d-11eb-1d73-d1488200d929
# ╠═9af3d034-8e2d-11eb-2689-af34d9c8e2c2
# ╠═33a960be-8e2e-11eb-356c-a3acc07cd298
# ╟─add792b6-8e2e-11eb-05d1-55c62c2d304b
# ╠═bc7e0156-8e2e-11eb-2ff0-1b2541453344
# ╠═803a09e6-8e2f-11eb-253b-dd7b881ab1f1
# ╠═ade3a26c-8e2f-11eb-278c-ef7f61b29b69
# ╠═d2b61cd2-8e2f-11eb-2f58-d948ddfc44b5
# ╠═0156ec06-8e30-11eb-30b6-630128d6bf1e
# ╠═14b4c110-8e30-11eb-0348-35cc6b6de5f0
# ╠═27b308a0-8e30-11eb-3594-6fb25629dc0d
# ╠═76848f60-8e30-11eb-34d7-5b31582a1018
# ╠═7d8780c2-8e30-11eb-29bb-59e96aff1864
# ╠═81dcd4a8-8e30-11eb-2117-b997a5862cc7
# ╠═8d61421e-8e30-11eb-00f8-a1787b433f6f
# ╠═93927ee6-8e30-11eb-0ae3-6de9bceede4d
# ╟─db729c96-8e30-11eb-2a29-3f18522feaa6
# ╠═e21b782e-8e30-11eb-1f58-f7891f730b67
# ╠═34cb49fa-8e31-11eb-252e-53fe2bc8048c
# ╠═099e5d02-8e32-11eb-2363-d3a94a868c54
# ╠═567c3458-8e32-11eb-1a87-71e4b2104382
# ╠═1cb987f4-8e32-11eb-0748-89445972edfb
# ╠═409ba742-8e32-11eb-043c-b755c332cf01
# ╠═49165c00-8e32-11eb-17e1-a321d7e0fc1f
# ╠═529996de-8e32-11eb-17b7-4d9923c4405b
# ╠═ff402106-8e31-11eb-0623-4bcd0f7e9c7d
# ╟─7f384348-8e32-11eb-33c5-3d8c444f1cec
# ╠═a7be5c6e-91e9-11eb-02d0-11a3b7351831
# ╠═1fb2fc88-91ed-11eb-3b45-f57c25dcb0d2
# ╠═5c57526a-91ed-11eb-3361-19c0cd550944
