### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 67f9bde8-1d6c-4bc8-a38c-638bbcd70511
using Pkg

# ╔═╡ 62fe8328-d602-49e5-9fb7-6c7a2c8e440b
Pkg.activate("Project.toml")

# ╔═╡ 98b7a698-9fb2-4241-bb84-f4eab7362245
using CSV

# ╔═╡ 2ce4fd1b-8d32-408d-879e-a8f4397f77d9
using DataFrames

# ╔═╡ 8e131fe4-85b6-44ce-b6e7-319c590b7e30
using PlutoUI

# ╔═╡ 3b4543e3-c336-4913-a235-dae865621148
using Statistics

# ╔═╡ 5a18acbb-e3a4-4641-a939-b2017859f5bb
using StatsBase

# ╔═╡ da142a9b-e3aa-462b-8cf3-2fc9717c80a8
using RDatasets

# ╔═╡ cab5d57e-ddab-4307-a103-fac9127cf71c
using Plots

# ╔═╡ c241f0a4-9735-4f9b-89ad-e4d1b683318b
using Flux

# ╔═╡ 7a7dc6e2-8615-4487-9720-c61232e4aa5b
using Flux.Data

# ╔═╡ ca92414e-3dff-47d6-af71-8b6a0a01dff7
using Flux: @epochs

# ╔═╡ df0296f4-b7ed-11eb-2900-ffb20d44c95f
md"# Linear regression model with ANNs"

# ╔═╡ fed3d59d-dc6e-481b-975c-e80fc0f94527
md"## Import libraries"

# ╔═╡ 315a33f6-7218-4200-8075-c74f513c5a71
md"## Load the dataset"

# ╔═╡ 9134297f-267d-4084-a528-fdcaaf4d0766
res_df = CSV.read("data/real_estate.csv", DataFrame)

# ╔═╡ 1a516c2c-c756-4152-9077-8ddc9d10ae3a
begin
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\[m\]"=>"_"))))
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\[s\]"=>"_"))))
	rename!(res_df, Symbol.(replace.(string.(names(res_df)), Ref(r"\s"=>"_"))))
end

# ╔═╡ ef777944-b3cd-4c3a-9295-af8a1f5c9158
md"## Prepare the model"

# ╔═╡ 86353cc1-6d44-43cc-b8c7-edc4c610b683
md"### Extract the training and testing the data"

# ╔═╡ f690a4bb-2c1b-44f3-a9f0-28b4eaec403e
X = res_df[:,3:7]

# ╔═╡ c0d4f485-4138-40b6-9a51-1cd2f8af1c0d
cor(Matrix(res_df))

# ╔═╡ d5a6c4bb-5f1d-453e-ac30-e633e3d5d7ad
another_data = Matrix(res_df[:,3:8])

# ╔═╡ af8e617e-8f73-4858-9e68-e01917ae0ccc
function get_scaling_params(init_feature_mat)
	feature_mean = mean(init_feature_mat, dims=1)
	f_dev = std(init_feature_mat, dims=1)
	return (feature_mean, f_dev)
end

# ╔═╡ d0d609d0-1182-41f2-8862-66abdbc15045
function scale_features(feature_mat, sc_params)
	normalised_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ ab95458b-06da-4977-ba98-0c70eb338369
X_mat = Matrix(X)

# ╔═╡ 9e8ce3ca-a77f-4fa7-a7d8-531764443f8c
Y = res_df[:,8]

# ╔═╡ d11d20d6-4c40-420e-b100-4aaeea293b13
all_data_size = size(X_mat)[1]

# ╔═╡ cb2796dd-d312-42d9-9aa9-06dd3a3a015e
training_size = 0.7

# ╔═╡ 12287d0c-ade7-420f-a934-e126192f88d4
training_index = trunc(Int, training_size * all_data_size)

# ╔═╡ f6028046-9649-432e-aaed-f9a0b1ef8fc9
another_train = another_data[1:training_index, :]

# ╔═╡ 9aea275f-fe32-4d56-879a-7904d195d32e
another_test = another_data[1+training_index:end, :]

# ╔═╡ d36b7118-7ee0-4b85-aaef-e455a5cc0f3c
sc_params = get_scaling_params(another_train[:, 1:end-1])

# ╔═╡ 0153a075-0c71-4a60-a24d-8d751ef25260
sc_mat_1 = scale_features(another_train[:, 1:end-1], sc_params)

# ╔═╡ a00f78bc-45a1-4e39-9add-4c31736f01d7
sc_mat_2 = scale_features(another_test[:, 1:end-1], sc_params)

# ╔═╡ 03a9420b-523d-4172-b413-c2513d01de4f
train_feat = transpose(sc_mat_1)

# ╔═╡ c82c8f93-a222-4588-99f2-7678b79d8541
test_feat = transpose(sc_mat_2)

# ╔═╡ 492641db-3d0f-4c0f-87c9-7e64344e7061
train_label = another_train[:, end]

# ╔═╡ da936cd4-fed6-4227-ae84-9f6402a6bc45
test_label = another_test[:, end]

# ╔═╡ ee0b62bd-12e0-4d5b-ae66-152fdcaec01c
size(test_label)

# ╔═╡ dcd22dae-8e2d-4f21-8bb3-9da9f9433742
dl_train = DataLoader((train_feat, train_label), batchsize=50)

# ╔═╡ 0a851af0-0529-4609-8e18-227cca59c49a
dl_test = DataLoader((test_feat, test_label), batchsize=10)

# ╔═╡ e9ca2c09-3a2a-44ba-b4a0-cb2f8db1dd02
X_mat_train = X_mat[1:training_index, :]

# ╔═╡ d22e7097-efd5-4d62-81f4-9c2faf46f8cc
size(X_mat_train)

# ╔═╡ 4846be57-9c9e-4131-8f32-9c67ad11de0c
X_mat_test = X_mat[1+training_index:end, :]

# ╔═╡ af6449c1-8205-4f2d-8d48-d5a4854911aa
Y_vec_train = Y[1:training_index]

# ╔═╡ bb928cd7-f096-4c89-be77-6c0b20b6818d
size(Y_vec_train)

# ╔═╡ f4b5777a-88c0-44ae-b95f-6236322b2d27
Y_vec_test = Y[1+training_index:end]

# ╔═╡ 6da9bc17-4210-4ff2-a4d1-bbaa28e85650
# re_m = Dense(5,1)
re_m = Chain(Dense(5,2,σ), Dense(2,1))

# ╔═╡ 6c6d5177-7631-4ba6-afa7-563cee31fc16

loss(x,y) = Flux.Losses.mse(re_m(x), y)

# ╔═╡ 8501c402-09da-44c3-9d8a-3b8b15c635f9
ps = Flux.params(re_m)

# ╔═╡ da559934-68a6-4dc5-9354-2ed3ad7c1c5e
opt = Descent(0.1)

# ╔═╡ a5672465-a7c2-4f9d-bb01-cf32533a4492
@epochs 30 Flux.train!(loss, ps, dl_train, opt)

# ╔═╡ 18585a0a-2124-4ee4-85ff-0f2f5624bd4f
pred2 = re_m(test_feat)

# ╔═╡ 81dc0f49-de17-4a4e-b228-508e04b53426
function root_mean_square_error(actual_outcome, predicted_outcome)
	errors = predicted_outcome - actual_outcome
	squared_errors = errors .^ 2
	mean_squared_errors = mean(squared_errors)
	rmse = sqrt(mean_squared_errors)
	return rmse
end

# ╔═╡ 2cdaabb2-26a7-407c-b2fb-2ba25bc33120
root_mean_square_error(test_label, vec(pred2))

# ╔═╡ Cell order:
# ╠═df0296f4-b7ed-11eb-2900-ffb20d44c95f
# ╠═fed3d59d-dc6e-481b-975c-e80fc0f94527
# ╠═67f9bde8-1d6c-4bc8-a38c-638bbcd70511
# ╠═62fe8328-d602-49e5-9fb7-6c7a2c8e440b
# ╠═98b7a698-9fb2-4241-bb84-f4eab7362245
# ╠═2ce4fd1b-8d32-408d-879e-a8f4397f77d9
# ╠═8e131fe4-85b6-44ce-b6e7-319c590b7e30
# ╠═3b4543e3-c336-4913-a235-dae865621148
# ╠═5a18acbb-e3a4-4641-a939-b2017859f5bb
# ╠═da142a9b-e3aa-462b-8cf3-2fc9717c80a8
# ╠═cab5d57e-ddab-4307-a103-fac9127cf71c
# ╠═c241f0a4-9735-4f9b-89ad-e4d1b683318b
# ╠═7a7dc6e2-8615-4487-9720-c61232e4aa5b
# ╠═ca92414e-3dff-47d6-af71-8b6a0a01dff7
# ╠═315a33f6-7218-4200-8075-c74f513c5a71
# ╠═9134297f-267d-4084-a528-fdcaaf4d0766
# ╠═1a516c2c-c756-4152-9077-8ddc9d10ae3a
# ╠═ef777944-b3cd-4c3a-9295-af8a1f5c9158
# ╠═86353cc1-6d44-43cc-b8c7-edc4c610b683
# ╠═f690a4bb-2c1b-44f3-a9f0-28b4eaec403e
# ╠═c0d4f485-4138-40b6-9a51-1cd2f8af1c0d
# ╠═d5a6c4bb-5f1d-453e-ac30-e633e3d5d7ad
# ╠═af8e617e-8f73-4858-9e68-e01917ae0ccc
# ╠═d0d609d0-1182-41f2-8862-66abdbc15045
# ╠═ab95458b-06da-4977-ba98-0c70eb338369
# ╠═9e8ce3ca-a77f-4fa7-a7d8-531764443f8c
# ╠═d11d20d6-4c40-420e-b100-4aaeea293b13
# ╠═cb2796dd-d312-42d9-9aa9-06dd3a3a015e
# ╠═12287d0c-ade7-420f-a934-e126192f88d4
# ╠═f6028046-9649-432e-aaed-f9a0b1ef8fc9
# ╠═9aea275f-fe32-4d56-879a-7904d195d32e
# ╠═d36b7118-7ee0-4b85-aaef-e455a5cc0f3c
# ╠═0153a075-0c71-4a60-a24d-8d751ef25260
# ╠═a00f78bc-45a1-4e39-9add-4c31736f01d7
# ╠═03a9420b-523d-4172-b413-c2513d01de4f
# ╠═c82c8f93-a222-4588-99f2-7678b79d8541
# ╠═492641db-3d0f-4c0f-87c9-7e64344e7061
# ╠═da936cd4-fed6-4227-ae84-9f6402a6bc45
# ╠═ee0b62bd-12e0-4d5b-ae66-152fdcaec01c
# ╠═dcd22dae-8e2d-4f21-8bb3-9da9f9433742
# ╠═0a851af0-0529-4609-8e18-227cca59c49a
# ╠═e9ca2c09-3a2a-44ba-b4a0-cb2f8db1dd02
# ╠═d22e7097-efd5-4d62-81f4-9c2faf46f8cc
# ╠═4846be57-9c9e-4131-8f32-9c67ad11de0c
# ╠═af6449c1-8205-4f2d-8d48-d5a4854911aa
# ╠═bb928cd7-f096-4c89-be77-6c0b20b6818d
# ╠═f4b5777a-88c0-44ae-b95f-6236322b2d27
# ╠═6da9bc17-4210-4ff2-a4d1-bbaa28e85650
# ╠═6c6d5177-7631-4ba6-afa7-563cee31fc16
# ╠═8501c402-09da-44c3-9d8a-3b8b15c635f9
# ╠═da559934-68a6-4dc5-9354-2ed3ad7c1c5e
# ╠═a5672465-a7c2-4f9d-bb01-cf32533a4492
# ╠═18585a0a-2124-4ee4-85ff-0f2f5624bd4f
# ╠═81dc0f49-de17-4a4e-b228-508e04b53426
# ╠═2cdaabb2-26a7-407c-b2fb-2ba25bc33120
