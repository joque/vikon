### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ df294fd6-88f1-4084-bf9f-0502f84388a7
using Pkg

# ╔═╡ 1fe14562-ca41-4f07-8ffb-f74f8dc31cc1
Pkg.activate("Project.toml")

# ╔═╡ a965c604-ad13-11eb-1abf-d3bdc31940f5
md"# Implementation of Constraint Satisfaction Problem"

# ╔═╡ 9a0d5f55-0732-4ade-84f9-6a970db236c8
@enum ColourDomain green red blue yellow

# ╔═╡ 3ea0c15a-9c1a-4874-b432-112376971b49
mutable struct CSPVar
	name::String
	value::Union{Nothing,ColourDomain}
	forbidden_values::Vector{ColourDomain}
	domain_restriction_count::Int64
end

# ╔═╡ 6ce1b89c-2358-4c82-bd39-1e019796ca25
struct ColourCSP
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end

# ╔═╡ b03ff5b5-0ee7-4a52-8c20-a71f9d26f6d0
col = rand(setdiff(Set([green,blue,yellow,red]), Set([red,green])))

# ╔═╡ 5425d0e8-e1dd-437f-ac79-73804e9f203f
function solve_csp(pb::ColourCSP, all_assignments)
	for cur_var in pb.vars
		if cur_var.domain_restriction_count == 4
			return []
		else
			next_val = rand(setdiff(Set([green,blue,yellow,red]), Set(cur_var.forbidden_values)))
			#assign the value to the variable
			cur_var.value = next_val
			
			#forward check
			for cur_constraint in pb.constraints
				if !((cur_constraint[1] == cur_var) || (cur_constraint[2] == cur_var))
					continue
				else
					if cur_constraint[1] == cur_var
						push!(cur_constraint[2].forbidden_values, next_val)
						cur_constraint[2].domain_restriction_count += 1
						# if the count reaches four you should backtrack
						# if the domain is a singleton (count == 3) propagate
					else
						push!(cur_constraint[1].forbidden_values, next_val)
						cur_constraint[1].domain_restriction_count += 1
						# if the count reaches four you should backtrack
						# if the domain is a singleton (count == 3) propagate
					end
				end
			end
			
			# add the assignment to all_assignments
			push!(all_assignments, cur_var.name => next_val)
		end
	end
	return all_assignments
end

# ╔═╡ dfcbf802-2fd5-47b9-9fcf-5bace63f0423
x1 = CSPVar("X1",nothing, [], 0)

# ╔═╡ 77c93299-1c33-4621-a086-945733f8609e
x2 = CSPVar("X2", nothing, [], 0)

# ╔═╡ 26d0b8a0-8ddc-4a1c-a060-ad382bb9dfe8
x3 = CSPVar("X3", nothing, [], 0)

# ╔═╡ d3002c3a-4c9d-4de6-84ff-586d00c7ecae
x4 = CSPVar("X4", nothing, [], 0)

# ╔═╡ 935de0f8-06d2-45a5-81cd-a203abdfab9e
x5 = CSPVar("X5", nothing, [], 0)

# ╔═╡ 9cfc10eb-43d9-4036-8e12-3b6efca28704
x6 = CSPVar("X6", nothing, [], 0)

# ╔═╡ f0db61d0-b3cd-4ca4-99ef-3622789a2605
x7 = CSPVar("X7", nothing, [], 0)

# ╔═╡ 7be2a7a5-0cab-4a1b-ae01-88788cdf21f6
problem = ColourCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4), (x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 927860d9-5c36-4975-887a-84958d6f3a5d
solve_csp(problem, [])

# ╔═╡ Cell order:
# ╠═a965c604-ad13-11eb-1abf-d3bdc31940f5
# ╠═df294fd6-88f1-4084-bf9f-0502f84388a7
# ╠═1fe14562-ca41-4f07-8ffb-f74f8dc31cc1
# ╠═9a0d5f55-0732-4ade-84f9-6a970db236c8
# ╠═3ea0c15a-9c1a-4874-b432-112376971b49
# ╠═6ce1b89c-2358-4c82-bd39-1e019796ca25
# ╠═b03ff5b5-0ee7-4a52-8c20-a71f9d26f6d0
# ╠═5425d0e8-e1dd-437f-ac79-73804e9f203f
# ╠═dfcbf802-2fd5-47b9-9fcf-5bace63f0423
# ╠═77c93299-1c33-4621-a086-945733f8609e
# ╠═26d0b8a0-8ddc-4a1c-a060-ad382bb9dfe8
# ╠═d3002c3a-4c9d-4de6-84ff-586d00c7ecae
# ╠═935de0f8-06d2-45a5-81cd-a203abdfab9e
# ╠═9cfc10eb-43d9-4036-8e12-3b6efca28704
# ╠═f0db61d0-b3cd-4ca4-99ef-3622789a2605
# ╠═7be2a7a5-0cab-4a1b-ae01-88788cdf21f6
# ╠═927860d9-5c36-4975-887a-84958d6f3a5d
